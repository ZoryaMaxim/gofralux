<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты: г. МОСКВА");
?>
<div class="container">
    <h1 class="h1">Контакты:<span class="h1__b h1__b_low"> г. МОСКВА</span></h1>
    <div class="map">
        <ul class="map-data">
            <li class="map-data__item map-data__item_map">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/c_moscow_address.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </li>
            <li class="map-data__item map-data__item_phone">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/c_moscow_phone.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </li>
            <li class="map-data__item map-data__item_mail">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/c_moscow_mail.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </li>
        </ul>
        <div class="map__map" data-lat="55.714369" data-lng="37.569104" data-zoom="16" data-address="119270, Москва, Лужнецкая набережная д. 10А стр. 4"></div>
        <article class="map__desc">
            <?$APPLICATION->IncludeFile(
                $APPLICATION->GetTemplatePath("include_areas/c_moscow_desc.php"),
                Array(),
                Array("MODE"=>"html")
            );?>
        </article>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
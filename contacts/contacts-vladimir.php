<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Контакты Владимир");
$APPLICATION->SetTitle("Контакты Владимир");
?>
<div class="container">
    <h1 class="h1">Контакты:<span class="h1__b h1__b_low"> г. Владимир, Владимирская область</span></h1>
    <div class="map">
        <ul class="map-data">
            <li class="map-data__item map-data__item_map">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/c_vladimir_address.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </li>
            <li class="map-data__item map-data__item_phone">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/c_vladimir_phone.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </li>
            <li class="map-data__item map-data__item_mail">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/c_vladimir_mail.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </li>
        </ul>
        <div class="map__container">
            <div class="map__map map__map_small" data-lat="56.119654" data-lng="40.359869" data-zoom="16" data-address="601240, Владимирская обл., г. Владимир, Проспект Ленина, дом 40"></div>
            <div class="map__vladimir">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/c_vladimir_img.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
        </div>
        <article class="map__desc">
            <?$APPLICATION->IncludeFile(
                $APPLICATION->GetTemplatePath("include_areas/c_vladimir_desc.php"),
                Array(),
                Array("MODE"=>"html")
            );?>
        </article>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<footer class="footer">
    <div class="footer__top">
        <div class="container container_contact">
            <div class="footer__left">
                <nav class="b-menu">
                    <p class="b-menu__caption"><a href="/catalog.php" class="b-menu__link b-menu__link_caption">Каталог продукции</a></p>
                    <ul class="b-menu__list">
                        <li class="b-menu__item"><a href="" class="b-menu__link">гофротара для...</a></li>
                        <li class="b-menu__item"><a href="" class="b-menu__link">4-х клапанные гофрокороба</a></li>
                        <li class="b-menu__item"><a href="" class="b-menu__link">Самосборные гофрокороба</a></li>
                        <li class="b-menu__item"><a href="" class="b-menu__link">Короба ГОСТ</a></li>
                        <li class="b-menu__item"><a href="" class="b-menu__link">Крупноформатные гофрокороба</a>
                        </li>
                        <li class="b-menu__item"><a href="" class="b-menu__link">Многоточечная склейка</a></li>
                        <li class="b-menu__item"><a href="" class="b-menu__link">Комплектующие к гофрокоробам</a>
                        </li>
                        <li class="b-menu__item"><a href="" class="b-menu__link">Прочие изделия</a></li>
                        <li class="b-menu__item"><a href="" class="b-menu__link">Гофрокартон в листах</a></li>
                    </ul>
                </nav>
                <nav class="b-menu">
                    <p class="b-menu__caption"><a href="" class="b-menu__link b-menu__link_caption">Дополнительные услуги</a></p>
                    <ul class="b-menu__list">
                        <li class="b-menu__item"><a href="" class="b-menu__link">Разработка конструкций</a></li>
                        <li class="b-menu__item"><a href="" class="b-menu__link">Изготовление штанц-форм</a></li>
                        <li class="b-menu__item"><a href="" class="b-menu__link">Изготовление печатных форм</a></li>
                        <li class="b-menu__item"><a href="" class="b-menu__link">Изготовление образцов</a></li>
                    </ul>
                    <p class="b-menu__caption b-menu__caption_last"><a href="" class="b-menu__link b-menu__link_caption">Производство</a></p>
                    <ul class="b-menu__list">
                        <li class="b-menu__item"><a href="/news.php" class="b-menu__link">Новости</a></li>
                        <li class="b-menu__item"><a href="/details.php" class="b-menu__link">Реквзиты</a></li>
                    </ul>
                </nav>
            </div>
            <div class="footer__right">
                <div class="f-contact">
                    <p class="f-contact__caption"><a href="moscow.php" class="b-menu__link b-menu__link_caption"><span class="f-contact__caption-contact">Контакты</span> Москва</a>
                    </p>
                    <div class="f-contact__data">
                        <span class="f-contact__data-hide">Телефон: </span>+7(495)600-46-12,<span
                            class="f-contact__data-hide"> E-mail: gofralux@mail.ru</span><br>
                        <span class="f-contact__data-hide">Адрес: </span>119270, Москва, Лужнецкая<br>
                        набережная д. 10А стр. 4
                    </div>
                </div>
                <div class="f-contact">
                    <p class="f-contact__caption"><a href="vladimir.php" class="b-menu__link b-menu__link_caption"><span class="f-contact__caption-contact">Контакты</span> Владимир</a>
                    </p>
                    <div class="f-contact__data">
                        <span class="f-contact__data-hide">Телефон:</span> +7 (49242)4-20-05<span
                            class="f-contact__data-hide"> E-mail: gofralux@mail.ru</span><br>
                        601240, Владимирская обл., г. Владимир,<br>
                        Проспект Ленина, дом 40
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="container container_footer">
            <p class="copyright">© 2011–<?php echo date('Y') ?> Гофралюкс Не является публичной офертой</p>
            <p class="g-lab"><a target="_blank" href="https://www.g-lab.ru/" class="g-lab__link">Разработка сайта
                    G-lab</a></p>
        </div>
    </div>

</footer>
</div>
<div id="price" class="modal">
    <a href="#close" class="modal__close-area"></a>
    <div class="modal__container">
        <a href="#close" class="modal__close m-form__close"></a>
        <form action="/" method="get" class="m-form">
            <div class="m-form__container">
                <p class="m-form__caption">Запрос рачета цены</p>
                <p class="m-form__sub"><span class="m-form__sub m-form__sub_b">Наименование: </span><span
                            class="m-form__sub m-form__sub_change"></span></p>
                <p class="m-form__sub m-form__sub_hide"><span class="m-form__sub m-form__sub_b">Цена за 1 шт: </span><span
                            class="m-form__sub m-form__sub_price"></span></p>
                <p class="m-form__row">
                    <label for="m-form__quantity" class="m-form__label">Необходимое количество</label>
                    <span class="m-form__input-wrapper">
                        <input type="number" min="1" step="1" value="1" class="m-form__input m-form__input_number" name="quantity" id="m-form__quantity" required>
                    </span>
                </p>
                <p class="m-form__row">
                    <label for="m-form__phone" class="m-form__label">Телефон</label>
                    <input type="text" class="m-form__input phone-mask" placeholder="+7 (___)-___-__-__" name="phone" id="m-form__phone">
                </p>
                <p class="m-form__row">
                    <label for="m-form__name" class="m-form__label">Имя</label>
                    <input type="text" class="m-form__input" name="name" id="m-form__name" required >
                </p>
                <input type="hidden" name="product-name" class="m-form__product-name">
                <input type="hidden" name="product-price" class="m-form__product-price" disabled>
                <input type="hidden" name="product-total" class="m-form__product-total" disabled>
                <p class="m-form__row m-form__row_hide">
                    <span class="m-form__total">Итого: </span>
                    <span class="m-form__total-input">123</span>
                </p>
                <p class="m-form__row m-form__row_submit"><button class="more-link more-link_button"><span class="more-link__text more-link__text_button">Заказать</span></button></p>
            </div>
        </form>
    </div>
</div>
<div id="thankyou" class="modal">
    <a href="#close" class="modal__close-area"></a>
    <div class="modal__container">
        <a href="#close" class="modal__close m-form__close"></a>
        <form action="#" method="post" class="m-form">
            <div class="m-form__container">
                <p class="m-form__caption">Спасибо</p><p class="m-form__text">Ваша заявка отправлена.</p> <p class="m-form__text">Мы свяжемся с Вами в ближайшее время.</p>

            </div>
        </form>
    </div>
</div>
<script defer src="/assets/dist/js/app.min.js"></script>
</body>
</html>
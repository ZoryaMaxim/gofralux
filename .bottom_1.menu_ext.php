<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;
if(CModule::IncludeModule("iblock")) {
    $arFilter = Array('IBLOCK_ID' => CATALOG_IB_ID, "SECTION_ID" => READY_PROD_SECTION_ID, "DEPTH_LEVEL"=>2, "ACTIVE"=>"Y");
    $db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, true);
    while($ar_result = $db_list->GetNext())
    {
        $aMenuLinksExt[] = Array(
            $ar_result['NAME'],
            $ar_result['SECTION_PAGE_URL'],
            Array(),
            Array(),
            ""
        );
    }
}
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);?>
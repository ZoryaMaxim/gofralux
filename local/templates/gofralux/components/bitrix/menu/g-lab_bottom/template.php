<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <ul class="b-menu__list">
        <?
        foreach($arResult as $arItem):
            if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;
        ?>
            <?if($arItem["SELECTED"]):?>
                <li class="b-menu__item"><a href="<?=$arItem["LINK"]?>" class="b-menu__link selected<?if($arItem["PARAMS"]["class"]){?> <?=$arItem["PARAMS"]["class"]?><?}?>"><?=$arItem["TEXT"]?></a></li>
            <?else:?>
                <li class="b-menu__item"><a href="<?=$arItem["LINK"]?>" class="b-menu__link<?if($arItem["PARAMS"]["class"]){?> <?=$arItem["PARAMS"]["class"]?><?}?>"><?=$arItem["TEXT"]?></a></li>
            <?endif?>
        <?endforeach?>
    </ul>
<?endif?>
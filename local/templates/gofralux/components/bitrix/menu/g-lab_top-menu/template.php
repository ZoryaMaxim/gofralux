<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$bIsMainPage=$APPLICATION->GetCurPage(false)==SITE_DIR;?>
<?if (!empty($arResult)):?>
<nav class="top-menu<?if($bIsMainPage){?> top-menu_home<?}?>">
	<ul class="top-menu__list">
        <?
        $previousLevel = 0;
        foreach($arResult as $arItem):?>
            <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
            <?endif?>
            <?if ($arItem["IS_PARENT"]):?>
                <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                    <li class="top-menu__item top-menu__item_top<?if($arItem["PARAMS"]["class"]){?> <?=$arItem["PARAMS"]["class"]?><?}?><?if(($arItem["LINK"]=='/catalog/')&&$bIsMainPage){?> top-menu__item_home<?}?><?if($arItem["SELECTED"]){?> active<?}?>">
                        <a href="<?=$arItem["LINK"]?>" class="top-menu__link<?if ($arItem["SELECTED"]):?> root-item-selected<?else:?> root-item<?endif?>"><?=$arItem["TEXT"]?></a>
                        <ul class="top-menu__sub">
                <?else:?>
                    <li class="top-menu__sub-item top-menu__sub-item_caption<?if($arItem["PARAMS"]["class"]){?> <?=$arItem["PARAMS"]["class"]?><?}?><?if ($arItem["SELECTED"]):?> item-selected<?endif?>">
                        <a href="<?=$arItem["LINK"]?>" class="top-menu__sub-link top-menu__sub-link_caption"><?=$arItem["TEXT"]?></a>
                        <ul class="third-menu">
                <?endif?>
            <?else:?>
                <?if ($arItem["PERMISSION"] > "D"):?>
                    <?if ($arItem["DEPTH_LEVEL"] == 1){?>
                        <li class="top-menu__item top-menu__item_top<?if($arItem["PARAMS"]["class"]){?> <?=$arItem["PARAMS"]["class"]?><?}?><?if($arItem["SELECTED"]){?> active<?}?>">
                            <a href="<?=$arItem["LINK"]?>" class="top-menu__link<?if ($arItem["SELECTED"]):?> root-item-selected<?else:?> root-item<?endif?>"><?=$arItem["TEXT"]?></a>
                        </li>
                    <?}elseif($arItem["DEPTH_LEVEL"] == 3){?>
                        <li class="third-menu__item<?if ($arItem["SELECTED"]):?> item-selected<?endif?>">
                            <a href="<?=$arItem["LINK"]?>" class="third-menu__link"><?=$arItem["TEXT"]?></a>
                        </li>
                    <?//кнопки под пунктами каталога
                    }elseif($arItem["PARAMS"]["CATALOG_BT"]=="Y"){?>
                        <li class="top-menu__sub-item top-menu__sub-item_blank">
                            <?$APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath("include_areas/top_menu_sub_catalog_bt.php"),
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                        </li>
                    <?}else{?>
                        <li class="top-menu__sub-item<?if ($arItem["SELECTED"]):?> item-selected<?endif?>">
                            <a href="<?=$arItem["LINK"]?>" class="top-menu__sub-link"><?=$arItem["TEXT"]?></a>
                        </li>
                    <?}?>
                <?else:?>
                    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                        <li><a href="" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
                    <?else:?>
                        <li><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
                    <?endif?>
               <?endif?>
            <?endif?>
            <?$previousLevel = $arItem["DEPTH_LEVEL"];?>
        <?endforeach?>
        <?if ($previousLevel > 1)://close last item tags?>
            <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
        <?endif?>
    </ul>
    <?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/top_menu_mobile_sub_catalog_bt.php"),
        Array(),
        Array("MODE"=>"html")
    );?>
</nav>
<?endif?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams["BLOCK_TITLE"]){?>
    <p class="h-production__title"><?=$arParams["~BLOCK_TITLE"]?></p>
<?}?>
<div class="h-production__list">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="h-production__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <figure class="h-production__image">

                <?if($arItem["PREVIEW_PICTURE"]){?>
                    <img
                        class="h-production__img"
                        border="0"
                        src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                        width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
                        height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
                        alt="<?echo $arItem["NAME"]?>"
                        title="<?echo $arItem["NAME"]?>"
                    />
                <?}else{?>
                    <img src="/assets/dist/img/nophoto_325_240.jpg">
                <?}?>

                <figcaption class="h-production__caption"><span class="h-production__caption-text">
                        <span class="h-production__caption-text h-production__caption-text_line"><?echo $arItem["NAME"]?></span>
                </figcaption>
            </figure>
        </div>
    <?endforeach;?>
</div>

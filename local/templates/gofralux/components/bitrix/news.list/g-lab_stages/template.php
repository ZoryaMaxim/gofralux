<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="stages">
    <div class="container container_stages">
        <?if($arParams["BLOCK_TITLE"]){?>
            <p class="stages__title"><?=($arParams["~BLOCK_TITLE"])?></p>
        <?}?>
        <ul class="stages__list">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <li class="stages__item<?if($arItem["PROPERTIES"]["ADDIT_CLASS"]["VALUE"]){echo " ".$arItem["PROPERTIES"]["ADDIT_CLASS"]["VALUE"];}?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="stages__item-text">
                        <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
                            <?echo $arItem["PREVIEW_TEXT"];?>
                        <?endif;?>
                    </div>
                </li>
            <?endforeach;?>
        </ul>
    </div>
</div>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
    <div class="h-news h-news_inner">
        <h1 class="h-news__caption"><?=GetMessage("NEWS_BLOCK_TITLE")?></h1>
        <?$i = 0;
        $closeDiv = false;
        foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $i++;
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            if($i == 1){
                $closeDiv = true;?>
                <div class="h-news__list">
            <?}elseif($i==5){
                $closeDiv = true;?>
                </div>
                <div class="h-news__list h-news__list_hide hide">
            <?}?>
            <div class="h-news__item tilter" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <figure class="h-news__figure tilter__figure">
                    <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="h-news__figure-wrap">
                        <?if($arItem["PREVIEW_PICTURE"]){?>
                            <img
                                class="h-news__img tilter__deco--overlay"
                                border="0"
                                src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                                width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
                                height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
                                alt="<?echo $arItem["NAME"]?>"
                                title="<?echo $arItem["NAME"]?>"
                            />
                        <?}else{?>
                            <img src="/assets/dist/img/nophoto.jpg" class="h-news__img tilter__deco--overlay">
                        <?}?>
                    </a>
                    <figcaption class="h-news__title tilter__caption">
                        <a class="h-news__link" href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
                    </figcaption>
                </figure>
                <div class="h-news__content">
                    <time datetime="<?echo FormatDate("Y-m-d", MakeTimeStamp($arItem["ACTIVE_FROM"]));?>" class="h-news__date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></time>
                    <div class="h-news__desc">
                        <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
                            <?echo $arItem["PREVIEW_TEXT"];?>
                        <?endif;?>
                    </div>
                </div>
            </div>
        <?endforeach;?>
        <?if($closeDiv){?>
            </div>
        <?}?>
        <?if(count($arResult["ITEMS"]) > 4){?>
            <a href="/news/" class="more-link more-link_news"><span class="more-link__text"><?=GetMessage("CT_BNL_ELEMENT_ALL_NEWS")?></span></a>
        <?}?>
    </div>
</div>

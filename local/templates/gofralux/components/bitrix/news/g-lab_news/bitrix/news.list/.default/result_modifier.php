<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as &$arItem):
    if(is_array($arItem["PREVIEW_PICTURE"])){
        $file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>400, 'height'=>265), BX_RESIZE_IMAGE_EXACT, true);
        $arItem["PREVIEW_PICTURE"]["SRC"] = $file['src'];
        $arItem["DETAIL_PICTURE"]["WIDTH"] = $file['width'];
        $arItem["DETAIL_PICTURE"]["HEIGHT"]= $file['height'];
    }
endforeach;?>
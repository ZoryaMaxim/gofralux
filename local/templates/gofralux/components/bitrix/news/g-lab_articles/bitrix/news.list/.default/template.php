<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="h-materials h-materials_inner">
	<div class="container">
        <h1 class="h-materials__caption">Полезные<span class="h-materials__caption h-materials__caption_b">материалы</span></h1>
			<?$i = 0;
            $closeDiv = false;
            foreach($arResult["ITEMS"] as $arItem):?>
				<?
                $i++;
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                if($i == 1){
                    $closeDiv = true;?>
                    <div class="h-materials__list">
                <?}elseif($i==4){
                    $closeDiv = true;?>
                    </div>
                    <div class="h-materials__list h-materials__list_hide hide">
                <?}?>
				<div class="h-materials__item tilter" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<figure class="h-materials__figure tilter__figure">
						<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="h-materials__figure-wrap">
							<?if($arItem["PREVIEW_PICTURE"]){?>
								<img
									class="h-materials__img tilter__deco--overlay"
									border="0"
									src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
									width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
									height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
									alt="<?echo $arItem["NAME"]?>"
									title="<?echo $arItem["NAME"]?>"
								/>
							<?}else{?>
								<img src="/assets/dist/img/nophoto_325_240.jpg" class="h-materials__img tilter__deco--overlay">
							<?}?>
						</a>
						<figcaption class="h-materials__title tilter__caption">
							<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="h-materials__link">
								<?echo $arItem["NAME"]?>
							</a>
						</figcaption>
					</figure>
					<div class="h-materials__content ">
						<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
							<?echo $arItem["PREVIEW_TEXT"];?>
						<?endif;?>
					</div>
				</div>
			<?endforeach;?>
		</div>
        <?if($closeDiv){?>
            </div>
        <?}?>
        <?if(count($arResult["ITEMS"]) > 3){?>
		    <a href="/articles/" class="more-link more-link_materials"><span class="more-link__text"><?=GetMessage("CT_BNL_ELEMENT_ALL_ARTICLES")?></span></a>
        <?}?>
	</div>
</div>

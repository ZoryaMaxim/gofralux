<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arViewModeList = array('LIST', 'LINE', 'TEXT', 'TILE');

$arDefaultParams = array(
	'VIEW_MODE' => 'LIST',
	'SHOW_PARENT_NAME' => 'Y',
	'HIDE_SECTION_NAME' => 'N'
);

$arParams = array_merge($arDefaultParams, $arParams);

if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
	$arParams['VIEW_MODE'] = 'LIST';
if ('N' != $arParams['SHOW_PARENT_NAME'])
	$arParams['SHOW_PARENT_NAME'] = 'Y';
if ('Y' != $arParams['HIDE_SECTION_NAME'])
	$arParams['HIDE_SECTION_NAME'] = 'N';

$arResult['VIEW_MODE_LIST'] = $arViewModeList;

if (0 < $arResult['SECTIONS_COUNT'])
{
	if ('LIST' != $arParams['VIEW_MODE'])
	{
		$boolClear = false;
		$arNewSections = array();
		foreach ($arResult['SECTIONS'] as &$arOneSection)
		{
			if (1 < $arOneSection['RELATIVE_DEPTH_LEVEL'])
			{
				$boolClear = true;
				continue;
			}
			$arNewSections[] = $arOneSection;
		}
		unset($arOneSection);
		if ($boolClear)
		{
			$arResult['SECTIONS'] = $arNewSections;
			$arResult['SECTIONS_COUNT'] = count($arNewSections);
		}
		unset($arNewSections);
	}
}

if (0 < $arResult['SECTIONS_COUNT'])
{
	$boolPicture = false;
	$boolDescr = false;
	$arSelect = array('ID');
	$arMap = array();
	if ('LINE' == $arParams['VIEW_MODE'] || 'TILE' == $arParams['VIEW_MODE'])
	{
		reset($arResult['SECTIONS']);
		$arCurrent = current($arResult['SECTIONS']);
		if (!isset($arCurrent['PICTURE']))
		{
			$boolPicture = true;
			$arSelect[] = 'PICTURE';
		}
		if ('LINE' == $arParams['VIEW_MODE'] && !array_key_exists('DESCRIPTION', $arCurrent))
		{
			$boolDescr = true;
			$arSelect[] = 'DESCRIPTION';
			$arSelect[] = 'DESCRIPTION_TYPE';
		}
	}
	if ($boolPicture || $boolDescr)
	{
		foreach ($arResult['SECTIONS'] as $key => $arSection)
		{
			$arMap[$arSection['ID']] = $key;
		}
		$rsSections = CIBlockSection::GetList(array(), array('ID' => array_keys($arMap)), false, $arSelect);
		while ($arSection = $rsSections->GetNext())
		{
			if (!isset($arMap[$arSection['ID']]))
				continue;
			$key = $arMap[$arSection['ID']];
			if ($boolPicture)
			{
				$arSection['PICTURE'] = intval($arSection['PICTURE']);
				$arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::GetFileArray($arSection['PICTURE']) : false);
				$arResult['SECTIONS'][$key]['PICTURE'] = $arSection['PICTURE'];
				$arResult['SECTIONS'][$key]['~PICTURE'] = $arSection['~PICTURE'];
			}
			if ($boolDescr)
			{
				$arResult['SECTIONS'][$key]['DESCRIPTION'] = $arSection['DESCRIPTION'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION'] = $arSection['~DESCRIPTION'];
				$arResult['SECTIONS'][$key]['DESCRIPTION_TYPE'] = $arSection['DESCRIPTION_TYPE'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION_TYPE'] = $arSection['~DESCRIPTION_TYPE'];
			}
		}
	}
}

$arSectionsID = array();
$arSectionsProp = array();
$arAdditSection = array();
$arAdditSections = array();
//ID пунктов меню
foreach ($arResult['SECTIONS'] as $arSection){
    $arSectionsID[] = $arSection["ID"];
}
//выбираем все секции для того чтобы получить доп свойства "UF_*"
$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], "ID"=>$arSectionsID, 'GLOBAL_ACTIVE'=>'Y');
$db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, false, array("ID","UF_*"));
while($ar_result = $db_list->Fetch()){
    $arSectionsProp[$ar_result["ID"]] = $ar_result;
}
//добавляем "UF_*" к пунктам
foreach ($arResult['SECTIONS'] as $key => $arSection){    
    $arResult['SECTIONS'][$key]["UF_BOX_ANIMATE"] =  $arSectionsProp[$arSection["ID"]]["UF_BOX_ANIMATE"];
    $arResult['SECTIONS'][$key]["UF_CAT_CLASS"]  = getBoxAnimateClass($arSectionsProp[$arSection["ID"]]["UF_BOX_ANIMATE"]);    
}
//последний пункт меню дополнительные услуги
$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], "ID"=>ADDIT_SERV_SECTION_ID , 'GLOBAL_ACTIVE'=>'Y');
$db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, false, array("ID", "NAME", "SECTION_PAGE_URL", "PICTURE", "UF_*"));
if($ar_result = $db_list->Fetch()){
    if($ar_result["PICTURE"]){
        $file = CFile::ResizeImageGet($ar_result["PICTURE"], array('width'=>160, 'height'=>120), BX_RESIZE_IMAGE_EXACT, true);
        $ar_result['PICTURE_SRC'] = $file['src'];
    }
    $ar_result["UF_CAT_CLASS"]  = getBoxAnimateClass($ar_result["UF_BOX_ANIMATE"]);
    $arAdditSection = $ar_result;
}
$arResult['ADDIT_SECTION'] = $arAdditSection;
//подразделы последнего пункта меню
$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], "SECTION_ID"=>ADDIT_SERV_SECTION_ID , 'GLOBAL_ACTIVE'=>'Y');
$db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, false, array("ID", "NAME", "SECTION_PAGE_URL", "PICTURE", "UF_*"));
while($ar_result = $db_list->Fetch()){
    $ar_result["UF_CAT_CLASS"]  = getBoxAnimateClass($ar_result["UF_BOX_ANIMATE"]);
    $arAdditSections[] = $ar_result;
} 
$arResult['ADDIT_SECTIONS'] = $arAdditSections;
?>
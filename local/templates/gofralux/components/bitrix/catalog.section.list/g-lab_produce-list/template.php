<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
	'LIST' => array(
		'CONT' => 'bx_sitemap',
		'TITLE' => 'bx_sitemap_title',
		'LIST' => 'bx_sitemap_ul',
	),
	'LINE' => array(
		'CONT' => 'bx_catalog_line',
		'TITLE' => 'bx_catalog_line_category_title',
		'LIST' => 'bx_catalog_line_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
	),
	'TEXT' => array(
		'CONT' => 'bx_catalog_text',
		'TITLE' => 'bx_catalog_text_category_title',
		'LIST' => 'bx_catalog_text_ul'
	),
	'TILE' => array(
		'CONT' => 'bx_catalog_tile',
		'TITLE' => 'bx_catalog_tile_category_title',
		'LIST' => 'bx_catalog_tile_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
	)
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];
$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>
<div class="container">
    <div class="produce">
        <div class="produce__caption">
            мы<span class="produce__caption produce__caption_b"> производим</span>
        </div>
        <?if(0 < $arResult["SECTIONS_COUNT"]){?>
            <div class="produce__list">
                <?$intCurrentDepth = 1;
                $boolFirst = true;
                $rightBoxAnimate = false;
                foreach ($arResult['SECTIONS'] as &$arSection){
                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                    /*
                    echo $arSection["NAME"]."<br />";
                    echo "intCurrentDepth ".$intCurrentDepth."<br />";
                    echo "RELATIVE_DEPTH_LEVEL ".$arSection['RELATIVE_DEPTH_LEVEL']."<br /><br />";*/
                    ?>
                    <?if ($intCurrentDepth < $arSection['RELATIVE_DEPTH_LEVEL'])
                    {
                        if (0 < $intCurrentDepth) {
                            //echo "\n",str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']),'<ul>';
                            if($rightBoxAnimate){
                                echo '<ul class="produce__sub produce__sub_right">';
                            }else{
                                echo '<ul class="produce__sub">';
                            }
                        }
                    }
                    elseif ($intCurrentDepth == $arSection['RELATIVE_DEPTH_LEVEL'])
                    {
                        if ($intCurrentDepth==1&&!$boolFirst){
                            echo "</div></div>";
                        }
                        if (!$boolFirst){
                            //echo '</li>';
                            //echo "</ul></div></div>";
                        }
                    }
                    else
                    {
                        /*
                        while ($intCurrentDepth > $arSection['RELATIVE_DEPTH_LEVEL'])
                        {
                            echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
                            $intCurrentDepth--;
                        }
                        //echo str_repeat("\t", $intCurrentDepth-1),'</li>';
                        */
                        if($rightBoxAnimate){
                            $rightBoxAnimate = false;
                        }
                        echo "</ul></div></div>";
                    }
                    //echo (!$boolFirst ? "\n" : ''),str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']);?>
                    <?if($arSection['RELATIVE_DEPTH_LEVEL'] == 1){
                        $arFilter = Array(
                            "IBLOCK_ID"=>$arParams["IBLOCK_ID"],
                            "SECTION_ID"=>$arSection["ID"]
                        );
                        $cnt = CIBlockSection::GetCount($arFilter);
                        //если анимация блока раскрытие вправо, то надо добавить в ul class
                        if(PRODUCE_RIGHT_ID == $arSection["UF_BOX_ANIMATE"]){
                            $rightBoxAnimate = true;
                        }

                    ?>
                        <div class="produce__item">
                            <div class="produce__inner<?if($arSection['UF_CAT_CLASS']){echo ' '.$arSection['UF_CAT_CLASS'];}?>" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
                                <a href="<?echo $arSection["SECTION_PAGE_URL"];?>" class="produce__link">
                                    <figure class="produce__figure<?if(($cnt==0)&&!$arSection["UF_SHOW_PRODUCTS"]){?> produce__figure_empty<?}?>">
                                        <img src="<?echo $arSection['PICTURE']['SRC']; ?>" alt="<? echo $arSection["NAME"];?>" class="produce__img">
                                        <figcaption class="produce__title"><? echo $arSection["NAME"];?></figcaption>
                                    </figure>
                                </a>
                                <?if($arSection["UF_SHOW_PRODUCTS"]&&count($arResult['SECTIONS_SHOW_PRODUCTS'][$arSection["ID"]])>0){?>
                                    <ul class="produce__sub">
                                        <?for($i=0;$i<count($arResult['SECTIONS_SHOW_PRODUCTS'][$arSection["ID"]]);$i++){?>
                                            <li class="produce__sub-item">
                                                <a href="<?=$arResult['SECTIONS_SHOW_PRODUCTS'][$arSection["ID"]][$i]["DETAIL_PAGE_URL"]?>" class="produce__sub-link"><?=$arResult['SECTIONS_SHOW_PRODUCTS'][$arSection["ID"]][$i]["NAME"]?></a>
                                            </li>
                                        <?}?>
                                    </ul>
                                <?}?>
                    <?}else{?>
                        <li class="produce__sub-item<?if($rightBoxAnimate){?> produce__sub-item_right<?}?>" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
                            <a href="<?echo $arSection["SECTION_PAGE_URL"];?>" class="produce__sub-link"><? echo $arSection["NAME"];?></a>
                        </li>
                    <?}?>
                    <?
                    $intCurrentDepth = $arSection['RELATIVE_DEPTH_LEVEL'];
                    $boolFirst = false;
                }
                unset($arSection);
                if ($intCurrentDepth > 1){?>
                    </ul></div></div>
                <?}elseif($intCurrentDepth == 1){?>
                    </div></div>
                <?}
                /*
                while ($intCurrentDepth > 1)
                {
                    //echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
                    $intCurrentDepth--;
                }
                if ($intCurrentDepth > 0)
                {
                    //echo '</li>',"\n";
                }
                */?>
                <?$rightBoxAnimate = false;
                if($arResult['ADDIT_SECTION']){
                    $arFilter = Array(
                        "IBLOCK_ID"=>$arParams["IBLOCK_ID"],
                        "SECTION_ID"=>$arResult['ADDIT_SECTION']["ID"]
                    );
                    $cnt = CIBlockSection::GetCount($arFilter);
                    //если анимация блока раскрытие вправо, то надо добавить в ul class
                    if(PRODUCE_RIGHT_ID == $arResult['ADDIT_SECTION']["UF_BOX_ANIMATE"]){
                        $rightBoxAnimate = true;
                    }?>
                    <div class="produce__item">
                        <div class="produce__inner<?if($arResult['ADDIT_SECTION']['UF_CAT_CLASS']){echo ' '.$arResult['ADDIT_SECTION']['UF_CAT_CLASS'];}?>">
                            <a href="/catalog/<?=$arResult['ADDIT_SECTION']["CODE"]?>/" class="produce__link">
                                <figure class="produce__figure<?if($cnt==0){?> produce__figure_empty<?}?>">
                                    <img src="<?=$arResult['ADDIT_SECTION']['PICTURE_SRC']?>" alt="<?=$arResult['ADDIT_SECTION']['NAME']?>" class="produce__img">
                                    <figcaption class="produce__title"><?=$arResult['ADDIT_SECTION']['NAME']?></figcaption>
                                </figure>
                            </a>
                            <?if((count($arResult['ADDIT_SECTIONS'])>0)||($arResult['ADDIT_SECTION']["UF_SHOW_PRODUCTS"]&&count($arResult['ADDIT_SECTIONS_SHOW_PRODUCTS'][$arResult['ADDIT_SECTION']["ID"]])>0)){?>
                                <ul class="produce__sub<?if($rightBoxAnimate){?> produce__sub_right<?}?>">
                                    <?foreach ($arResult['ADDIT_SECTIONS'] as $arSection){?>
                                        <li class="produce__sub-item<?if($rightBoxAnimate){?> produce__sub-item_right<?}?>"><a href="/catalog/<?=$arSection["CODE"]?>/" class="produce__sub-link"><?=$arSection["NAME"]?></a></li>
                                    <?}?>
                                    <?for($i=0;$i<count($arResult['ADDIT_SECTIONS_SHOW_PRODUCTS'][$arResult['ADDIT_SECTION']["ID"]]);$i++){?>
                                        <li class="produce__sub-item">
                                            <a href="<?=$arResult['ADDIT_SECTIONS_SHOW_PRODUCTS'][$arResult['ADDIT_SECTION']["ID"]][$i]["DETAIL_PAGE_URL"]?>" class="produce__sub-link"><?=$arResult['ADDIT_SECTIONS_SHOW_PRODUCTS'][$arResult['ADDIT_SECTION']["ID"]][$i]["NAME"]?></a>
                                        </li>
                                    <?}?>
                                </ul>
                            <?}?>
                            <?/*if($arResult['ADDIT_SECTION']["UF_SHOW_PRODUCTS"]&&count($arResult['ADDIT_SECTIONS_SHOW_PRODUCTS'][$arResult['ADDIT_SECTION']["ID"]])>0){?>
                                <ul class="produce__sub">
                                    <?for($i=0;$i<count($arResult['ADDIT_SECTIONS_SHOW_PRODUCTS'][$arResult['ADDIT_SECTION']["ID"]]);$i++){?>
                                        <li class="produce__sub-item">
                                            <a href="<?=$arResult['ADDIT_SECTIONS_SHOW_PRODUCTS'][$arResult['ADDIT_SECTION']["ID"]][$i]["DETAIL_PAGE_URL"]?>" class="produce__sub-link"><?=$arResult['ADDIT_SECTIONS_SHOW_PRODUCTS'][$arResult['ADDIT_SECTION']["ID"]][$i]["NAME"]?></a>
                                        </li>
                                    <?}?>
                                </ul>
                            <?}*/?>
                        </div>
                    </div>
                <?}?>
            </div>
        <?}?>
    </div>
</div>

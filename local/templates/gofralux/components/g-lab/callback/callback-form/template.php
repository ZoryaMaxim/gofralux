<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<?if(!empty($arResult["ERROR_MESSAGE"])){?>
    <?
    $err = '';
    foreach($arResult["ERROR_MESSAGE"] as $v){
        //ShowError($v);
        $err .= '<p class="m-form__text">'.$v.'</p>';
    }
    ?>
    <script>
        formAnswer('<p class="m-form__caption m-form__caption_error">Ошибка!</p><?=$err?>');
        addPhoneMask($);
        validateForm();
    </script>
<?}?>
<?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
    <script>
        formAnswer('<p class="m-form__caption">Спасибо!</p><p class="m-form__text">Ваша заявка отправлена.</p><p class="m-form__text">Мы обязательно с Вами свяжемся в ближайшее время.</p>');
        addPhoneMask($);
        validateForm();
    </script>
<?}?>
<form class="head-phone" action="<?=POST_FORM_ACTION_URI?>" method="POST">
    <?=bitrix_sessid_post()?>
    <div class="head-phone__inner">
        <label for="head-phone__input" class="head-phone__label">Закажите звонок</label>
        <input placeholder="+7 (___)-___-__-__" type="text" name="phone" id="head-phone__input" class="head-phone__input phone-mask" required>
    </div>
    <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
    <button class="head-phone__button" type="submit" name="submit" value="Y"></button>
</form>

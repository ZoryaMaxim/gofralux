<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */


$dir = $APPLICATION->GetCurDir();
$url = explode("/", $dir);
if(count($url)>0) {
    unset($url[count($url) - 1]);
    unset($url[0]);
    $url = array_values($url);
    if($url[0]=="catalog" && count($url) == 2) {
        $bIsCatalogListPage = true;
    }
    if($url[0]=="catalog" && count($url) == 3) {
        $bIsCatalogDetailPage = true;
    }
}

?>
<?/*
<pre>
    <?print_r($arResult)?>
</pre>
*/?>
<?if(!empty($arResult["ERROR_MESSAGE"])){?>
    <?
    $err = '';
    foreach($arResult["ERROR_MESSAGE"] as $v){
        //ShowError($v);
        $err .= '<p class="m-form__text">'.$v.'</p>';
    }
    ?>
    <script>
        <?if($bIsCatalogListPage){?>
            quantityCalc();
        <?}?>
        modalAnswer('<p class="m-form__caption m-form__caption_error">Ошибка!</p><?=$err?><a href="#" class="more-link more-link_error"><span class="more-link__text">Ввести заново</span></a>');
        addPhoneMask($);
        validateForm();
    </script>
<?}?>
<?if(strlen($arResult["OK_MESSAGE"]) > 0){
    ?>
    <script>
        modalAnswer('<p class="m-form__caption">Спасибо</p><p class="m-form__text">Благодарим за заказ.</p> <p class="m-form__text">Мы обязательно с Вами свяжемся в ближайшее время.</p>');
        addPhoneMask($);
        validateForm();
    </script>
<?}?>
<form class="m-form" action="<?=POST_FORM_ACTION_URI?>" method="POST">
    <?=bitrix_sessid_post()?>
    <div class="m-form__container">
        <p class="m-form__caption"><?if($bIsCatalogDetailPage){?>Запрос раcчета цены<?}else{?>Состав быстрого заказа<?}?></p>
        <p class="m-form__sub">
            <span class="m-form__sub m-form__sub_b">Наименование: </span>
            <span class="m-form__sub m-form__sub_change"><?=$arResult["PRODUCT_NAME"]?></span>
        </p>
        <p class="m-form__sub m-form__sub_hide<?if($arResult["PRODUCT_PRICE"]){?> show<?}?>">
            <span class="m-form__sub m-form__sub_b">Цена за 1 шт: </span>
            <span class="m-form__sub m-form__sub_price"><?=$arResult["PRODUCT_PRICE"]?></span>
        </p>
        <p class="m-form__row">
            <label for="m-form__quantity" class="m-form__label">Необходимое количество</label>
            <span class="m-form__input-wrapper">
                <input type="number" min="1" step="1" value="<?if($arResult["PRODUCT_QUANTITY"]){echo $arResult["PRODUCT_QUANTITY"];}else{echo 1;}?>" class="m-form__input m-form__input_number" name="quantity" id="m-form__quantity" required>
            </span>
        </p>
        <p class="m-form__row">
            <label for="m-form__phone" class="m-form__label">Телефон</label>
            <input type="text" class="m-form__input phone-mask" value="<?=$arResult["PHONE"]?>" placeholder="+7 (___)-___-__-__" name="phone" id="m-form__phone" required>
        </p>
        <p class="m-form__row">
            <label for="m-form__name" class="m-form__label">Имя</label>
            <input type="text" class="m-form__input" value="<?=$arResult["NAME"]?>" name="name" id="m-form__name" required >
        </p>
        <input type="hidden" name="product-name" value="<?=$arResult["PRODUCT_NAME"]?>" class="m-form__product-name">
        <input type="hidden" name="product-price" value="<?=$arResult["PRODUCT_PRICE"]?>" class="m-form__product-price" <?if(!$arResult["PRODUCT_PRICE"]){?>disabled<?}?>>
        <input type="hidden" name="product-total" value="<?=$arResult["PRODUCT_TOTAL"]?>" class="m-form__product-total" <?if(!$arResult["PRODUCT_TOTAL"]){?>disabled<?}?>>
        <input type="hidden" name="product-id" value="<?=$arResult["PRODUCT_ID"]?>" class="m-form__product-id" >
        <p class="m-form__row m-form__row_hide<?if($arResult["PRODUCT_TOTAL"]){?> show<?}?>">
            <span class="m-form__total">Итого: </span>
            <span class="m-form__total-input"><?=$arResult["PRODUCT_TOTAL"]?> р.</span>
        </p>
        <p class="m-form__row m-form__row_submit">
            <button class="more-link more-link_button" type="submit" name="submit" value="Y"><span class="more-link__text more-link__text_button">Заказать</span></button>
        </p>
    </div>
    <div class="m-form__answer"></div>
    <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
    <?/*if($arParams["USE_CAPTCHA"] == "Y"):?>
        <div class="mf-captcha">
            <div class="mf-text"><?=GetMessage("MFPF_CAPTCHA")?></div>
            <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
            <div class="mf-text"><?=GetMessage("MFPF_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
            <input type="text" name="captcha_word" size="30" maxlength="50" value="">
        </div>
    <?endif;*/?>
</form>

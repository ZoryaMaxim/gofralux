<?
$MESS ['MFPF_NAME'] = "Как к Вам обращаться";
$MESS ['MFPF_PHONE'] = "Телефон";
$MESS ['MFPF_COMP_NAME'] = "Название компании";
$MESS ['MFPF_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFPF_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFPF_SUBMIT_MODAL'] = "Отправить";

$MESS ['MFPF_COMP_MESSAGE'] = "Суть вопроса";
$MESS ['MFPF_SUBJECTS'] = "Тема вопроса:";
$MESS ['MFPF_TRADING'] = "Оптовая торговля";
$MESS ['MFPF_RETAIL'] = "Розничная торговля";
$MESS ['MFPF_CLAIM'] = "Претензия";
$MESS ['MFPF_ACCOUNTING'] = "Бухгалтерия";
$MESS ['MFPF_OTHER'] = "Другое";
?>
<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<?if(!empty($arResult["ERROR_MESSAGE"])){?>
    <?
    $err = '';
    foreach($arResult["ERROR_MESSAGE"] as $v){
        //ShowError($v);
        $err .= '<p class="m-form__text">'.$v.'</p>';
    }
    ?>
    <script>
        formAnswer('<p class="m-form__caption m-form__caption_error">Ошибка!</p><?=$err?>');
        addPhoneMask($);
        validateForm();
    </script>
<?}?>
<?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
    <script>
        formAnswer('<p class="m-form__caption">Спасибо!</p><p class="m-form__text">Благодарим за заказ</p><p class="m-form__text">Мы обязательно с Вами свяжемся в ближайшее время</p>');
        addPhoneMask($);
        validateForm();
    </script>
<?}?>
<form class="f-ordering" action="<?=POST_FORM_ACTION_URI?>" method="POST">
    <?=bitrix_sessid_post()?>
    <div class="f-ordering__column f-ordering__hide">
        <p class="f-ordering__row">
            <label for="f-ordering__type" class="f-ordering__label">Тип короба</label>
            <span class="f-ordering__select-wrap">
                <select name="type" id="f-ordering__type" class="f-ordering__select">
                    <option value="4-х клапанный">4-х клапанный</option>
                    <option value="Самосборный">Самосборный</option>
                    <option value="ГОСТ">ГОСТ</option>
                    <option value="FEFCO">FEFCO</option>
                    <option value="Со сложной высечкой">Со сложной высечкой</option>
                </select>
            </span>
        </p>
        <p class="f-ordering__row">
            <label class="f-ordering__label">Размеры (Д - Ш - В)</label>
            <span class="f-ordering__row-inner">
                <input type="text" name="length" class="f-ordering__input f-ordering__input_small">
                <input type="text" name="width" class="f-ordering__input f-ordering__input_small">
                <input type="text" name="height" class="f-ordering__input f-ordering__input_small">
            </span>
        </p>
        <p class="f-ordering__row">
            <label for="mark_name" class="f-ordering__label">Марка картона</label>
            <span class="f-ordering__row-inner">
                <?/*<input type="text" name="mark_name" id="mark_name" class="f-ordering__input f-ordering__input_middle">*/?>
                <span class="f-ordering__select-wrap f-ordering__select-wrap_middle">
                    <select name="mark_name" class="f-ordering__select">
                        <option value="Т-21">Т-21</option>
                        <option value="Т-22">Т-22</option>
                        <option value="Т-23">Т-23</option>
                        <option value="Т-24">Т-24</option>
                        <option value="П-31">П-31</option>
                        <option value="П-32">П-32</option>
                        <option value="МГК">МГК</option>
                    </select>
                </span>
                <span class="f-ordering__select-wrap f-ordering__select-wrap_middle">
                    <select name="mark_type" class="f-ordering__select">
                        <option value="Бурый">Бурый</option>
                        <option value="Серый">Серый</option>
                        <option value="Белый">Белый</option>
                    </select>
                </span>
            </span>
        </p>
        <p class="f-ordering__row">
            <label for="f-ordering__profile" class="f-ordering__label">Профиль картона</label>
            <span class="f-ordering__select-wrap">
                <select name="profile" id="f-ordering__profile" class="f-ordering__select">
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="BC">BC</option>
                    <option value="E">E</option>
                    <option value="Двухслойный">Двухслойный</option>
                </select>
            </span>
        </p>
        <p class="f-ordering__row">
            <label for="f-ordering__print" class="f-ordering__label">Печать</label>
            <span class="f-ordering__select-wrap">
                <select name="print" id="f-ordering__print" class="f-ordering__select">
                    <option value="Да">Да</option>
                    <option value="Нет">Нет</option>
                </select>
            </span>
        </p>
        <p class="f-ordering__row">
            <?/*<input type="text" name="color" id="f-ordering__color" class="f-ordering__input">*/?>
            <label for="f-ordering__color" class="f-ordering__label">Количество цветов</label>
            <span class="f-ordering__select-wrap">
                <select name="color" id="f-ordering__color" class="f-ordering__select">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </span>
        </p>
        <p class="f-ordering__row">
            <label for="f-ordering__square" class="f-ordering__label">Площадь запечатки</label>
            <input type="text" name="square" id="f-ordering__square" class="f-ordering__input">
        </p>
    </div>
    <div class="f-ordering__column">
        <p class="f-ordering__row f-ordering__hide">
            <label for="f-ordering__circulation" class="f-ordering__label">Тираж</label>
            <input type="text" name="circulation" id="f-ordering__circulation"
                   class="f-ordering__input">
        </p>
        <p class="f-ordering__row f-ordering__hide">
            <label for="f-ordering__comment" class="f-ordering__label">Комментарий</label>
            <input type="text" name="comment" id="f-ordering__comment" class="f-ordering__input">
        </p>
        <p class="f-ordering__row">
            <label for="f-ordering__org" class="f-ordering__label">Организация</label>
            <input type="text" name="org" id="f-ordering__org" class="f-ordering__input">
        </p>
        <p class="f-ordering__row">
            <label for="f-ordering__contact" class="f-ordering__label">Контактное лицо</label>
            <input type="text" name="contact" required id="f-ordering__contact"
                   class="f-ordering__input">
        </p>
        <p class="f-ordering__row">
            <label for="f-ordering__email" class="f-ordering__label">E-mail</label>
            <input type="email" name="email" required id="f-ordering__email"
                   class="f-ordering__input">
        </p>
        <p class="f-ordering__row">
            <label for="f-ordering__phone" class="f-ordering__label">Телефон</label>
            <input type="text" name="phone" placeholder="+7 (___)-___-__-__" id="f-ordering__phone"
                   class="phone-mask f-ordering__input">
        </p>
        <p class="f-ordering__row f-ordering__row_submit">
            <button class="f-ordering__submit" type="submit" name="submit" value="Y"><span
                    class="f-ordering__submit-text">отправить</span></button>
        </p>
    </div>
    <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
    <?/*if($arParams["USE_CAPTCHA"] == "Y"):?>
        <div class="mf-captcha">
            <div class="mf-text"><?=GetMessage("MFPF_CAPTCHA")?></div>
            <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
            <div class="mf-text"><?=GetMessage("MFPF_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
            <input type="text" name="captcha_word" size="30" maxlength="50" value="">
        </div>
    <?endif;*/?>
</form>
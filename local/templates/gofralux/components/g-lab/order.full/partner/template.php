<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<form class="form-partners" action="<?=POST_FORM_ACTION_URI?>" method="POST">
    <p>Стать нашим партнером, узнать оптовые цены или связаться с нами вы можете, заполнив данную форму.</p>
    <?if(!empty($arResult["ERROR_MESSAGE"])){
        foreach($arResult["ERROR_MESSAGE"] as $v)
            ShowError($v);
    }
    if(strlen($arResult["OK_MESSAGE"]) > 0){?>
        <p class="ok"><?=$arResult["OK_MESSAGE"]?></p>
    <?}?>
    <?=bitrix_sessid_post()?>
    <input class="authorization__input" type="text" name="company_name" placeholder="<?=GetMessage("MFT_COMP_NAME")?>" value="<?//=$arResult["COMPANY_NAME"]?>" <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("COMPANY_NAME", $arParams["REQUIRED_FIELDS"])):?>required="required"<?endif?>>
    <input class="authorization__input" type="text" name="user_name" placeholder="<?=GetMessage("MFT_NAME")?>" value="<?//=$arResult["AUTHOR_NAME"]?>" <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?>required="required"<?endif?>>
    <input class="authorization__input" type="tel" name="user_phone" placeholder="<?=GetMessage("MFT_PHONE")?>" value="<?//=$arResult["AUTHOR_PHONE"]?>" <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])):?>required="required"<?endif?>>
    <?/*if($arParams["USE_CAPTCHA"] == "Y"):?>
        <div class="mf-captcha">
            <div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
            <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
            <div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
            <input type="text" name="captcha_word" size="30" maxlength="50" value="">
        </div>
    <?endif;*/?>
    <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
    <button type="submit" name="submit" class="authorization__btn btn__partners" value="<?=GetMessage("MFT_SUBMIT")?>"><?=GetMessage("MFT_SUBMIT")?></button>
</form>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="icon" type="image/png" sizes="16x16" href="<?=SITE_DIR?>assets/dist/favicon/favicon.ico">
        <meta name="msapplication-TileColor" content="#ffcc33">
        <meta name="theme-color" content="#ffcc33">
        <?$APPLICATION->ShowHead();?>
        <title><?$APPLICATION->ShowTitle()?></title>
        <script data-skip-moving=true>
            (function () {
                if (sessionStorage.foutFontsLoaded) {
                    document.documentElement.className += " fonts-loaded";
                    return;
                }
                <?php require_once($_SERVER["DOCUMENT_ROOT"]."/assets/src/js/fontfaceobserver.standalone.js");?>
                <?php require_once($_SERVER["DOCUMENT_ROOT"]."/assets/dist/js/fonts.min.js");?>
            })();
        </script>
        <style><?php require_once($_SERVER["DOCUMENT_ROOT"]."/assets/dist/css/head.min.css");?></style>
    </head>
    <?$bIsMainPage=$APPLICATION->GetCurPage(false)==SITE_DIR;
    $bIsCatalogPage=$APPLICATION->GetCurPage(false)=="/catalog/";
    $bIsNewsDetailPage = false;
    $bIsArticleDetailPage = false;
    $bIsCatalogListPage = false;
    $bIsCatalogDetailPage = false;
    $dir = $APPLICATION->GetCurDir();
    if($dir=="/contacts/"){
        $bIsContactsPage= true;
    }else{
        $bIsContactsPage= false;
    }
    $url = explode("/", $dir);
    if(count($url)>0) {
        unset($url[count($url) - 1]);
        unset($url[0]);
        $url = array_values($url);
        if($url[0]=="news" && count($url) == 2) {
            $bIsNewsDetailPage = true;
        }
        if($url[0]=="articles" && count($url) == 2) {
            $bIsArticleDetailPage = true;
        }
        if($url[0]=="catalog" && count($url) == 2) {
            $bIsCatalogListPage = true;
        }
        if($url[0]=="catalog" && count($url) == 3) {
            $bIsCatalogDetailPage = true;
        }
    }
    ?>
    <body>
        <?$APPLICATION->ShowPanel()?>
        <div id="wrapper" class="wrapper">
            <main class="main">
                <div class="main__header<?if($bIsMainPage){?> main__header_home<?}?>">
                    <header class="header">
                        <div class="header__top<?if($bIsMainPage){?> header__top_home<?}?>">
                            <div class="container container_top">
                                <div class="burger">
                                    <div class="burger__btn"><span></span></div>
                                </div>
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "g-lab_top-menu",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                                        "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                                        "MAX_LEVEL" => "2",	// Уровень вложенности меню
                                        "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                                            0 => "",
                                        ),
                                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                        "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                        "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                                    ),
                                    false
                                );?>
                                <div class="logo"><a href="/" class="logo__link"></a></div>
                                <ul class="top-phone">
                                    <li class="top-phone__item">
                                        <?$APPLICATION->IncludeFile(
                                            $APPLICATION->GetTemplatePath("include_areas/top_phone_it_1.php"),
                                            Array(),
                                            Array("MODE"=>"html")
                                        );?>
                                    </li>
                                    <li class="top-phone__item">
                                        <?$APPLICATION->IncludeFile(
                                            $APPLICATION->GetTemplatePath("include_areas/top_phone_it_2.php"),
                                            Array(),
                                            Array("MODE"=>"html")
                                        );?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="container container_head">                            
                            <?$APPLICATION->IncludeComponent(
                                "g-lab:callback",
                                "callback-form",
                                Array(
                                    "EMAIL_TO" => "gp@g-lab.ru",
                                    "EVENT_MESSAGE_ID" => array("10"),
                                    "OK_TEXT" => "Спасибо, ваша заявка принята.",
                                    "AJAX_MODE" => "Y",  // режим AJAX
                                    "AJAX_OPTION_SHADOW" => "N", // затемнять область
                                    "AJAX_OPTION_JUMP" => "N", // скроллить страницу до компонента
                                    "AJAX_OPTION_STYLE" => "N", // подключать стили
                                    "AJAX_OPTION_HISTORY" => "N",
                                )
                            );?>
                            <div class="head-phone head-phone_mail">
                                <div class="head-phone__inner head-phone__inner_mail">
                                    <a href="mailto:gofralux@mail.ru" class="head-phone__mail">gofralux@mail.ru</a>
                                </div>
                                <a href="mailto:gofralux@mail.ru" class="head-phone__button head-phone__button_mail"></a>
                            </div>
                            <a href="tel:+74956004612" class="m-head-phone<?if($bIsMainPage){?> m-head-phone_home<?}?>"></a>
                            <a href="mailto:gofralux@mail.ru" class="m-head-phone m-head-phone_mail<?if($bIsMainPage){?> m-head-phone_home<?}?>"></a>
                        </div>
                    </header>
                    <?if($bIsMainPage){?>
                        <div class="container container_video">
                            <div class="text-block">
                                <h1 class="text-block__caption">
                                    <?$APPLICATION->IncludeFile(
                                        $APPLICATION->GetTemplatePath("include_areas/header_h1_row_1.php"),
                                        Array(),
                                        Array("MODE"=>"html")
                                    );?> <br>
                                    <span class="text-block__caption_bold">
                                        <?$APPLICATION->IncludeFile(
                                            $APPLICATION->GetTemplatePath("include_areas/header_h1_row_2_1.php"),
                                            Array(),
                                            Array("MODE"=>"html")
                                        );?>
                                    </span>
                                    <span class="text-block__caption_small">
                                        <?$APPLICATION->IncludeFile(
                                            $APPLICATION->GetTemplatePath("include_areas/header_h1_row_2_2.php"),
                                            Array(),
                                            Array("MODE"=>"html")
                                        );?>
                                    </span>
                                </h1>
                                <div class="text-block__normal">
                                    <?$APPLICATION->IncludeFile(
                                        $APPLICATION->GetTemplatePath("include_areas/header_t_block_normal.php"),
                                        Array(),
                                        Array("MODE"=>"html")
                                    );?>
                                </div>
                                <div class="text-block__small">
                                    <div class="text-block__small_st">
                                        <?$APPLICATION->IncludeFile(
                                            $APPLICATION->GetTemplatePath("include_areas/header_text-block_small_st.php"),
                                            Array(),
                                            Array("MODE"=>"html")
                                        );?>
                                    </div>
                                    <div class="text-block__small_nd">
                                        <?$APPLICATION->IncludeFile(
                                            $APPLICATION->GetTemplatePath("include_areas/header_text-block_small_nd.php"),
                                            Array(),
                                            Array("MODE"=>"html")
                                        );?>
                                    </div>
                                    <div class="text-block__small_rd">
                                        <?$APPLICATION->IncludeFile(
                                            $APPLICATION->GetTemplatePath("include_areas/header_text-block_small_rd.php"),
                                            Array(),
                                            Array("MODE"=>"html")
                                        );?>
                                    </div>
                                </div>
                                <span class="text-block__scroll"></span>
                            </div>
                        </div>
                        <div class="video">
                            <video autoplay loop muted src="/assets/dist/video/gofralux.mp4" class="video__item"></video>
                        </div>
                    <?}?>
                </div>
                <?if($bIsMainPage){?>
                    <div class="container">
                        <div class="h-production">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:news.list",
                                "g-lab_production-list-main",
                                array(
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_TYPE" => "A",
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "BLOCK_TITLE" => "Наше <b class=\"h-production__title h-production__title_b\">производство</b>",
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "FIELD_CODE" => array(
                                        0 => "DETAIL_PICTURE",
                                        1 => "",
                                    ),
                                    "FILTER_NAME" => "",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "IBLOCK_ID" => "5",
                                    "IBLOCK_TYPE" => "information_materials",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "Y",
                                    "MESSAGE_404" => "",
                                    "NEWS_COUNT" => "2",
                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => ".default",
                                    "PAGER_TITLE" => "",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "PROPERTY_CODE" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "SET_BROWSER_TITLE" => "N",
                                    "SET_LAST_MODIFIED" => "N",
                                    "SET_META_DESCRIPTION" => "N",
                                    "SET_META_KEYWORDS" => "N",
                                    "SET_STATUS_404" => "N",
                                    "SET_TITLE" => "N",
                                    "SHOW_404" => "N",
                                    "SORT_BY1" => "SORT",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER1" => "ASC",
                                    "SORT_ORDER2" => "ASC",
                                    "COMPONENT_TEMPLATE" => "g-lab_production-list-main"
                                ),
                                false
                            );?>
                            <div class="h-production__desc">
                                <?$APPLICATION->IncludeFile(
                                    $APPLICATION->GetTemplatePath("include_areas/production_desc_main.php"),
                                    Array(),
                                    Array("MODE"=>"html")
                                );?>
                            </div>
                            <a href="#" class="more-link more-link_production"><span class="more-link__text">Узнать больше</span></a>
                        </div>
                    </div>    
                <?}?>
                <?/*if($bIsMainPage){?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "g-lab_stages",
                        Array(
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                            "AJAX_MODE" => "N",	// Включить режим AJAX
                            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                            "CACHE_TYPE" => "A",	// Тип кеширования
                            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                            "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                            "BLOCK_TITLE" => "этапы<span class=\"stages__title_b\"> производственного процесса</span>",
                            "DISPLAY_DATE" => "N",	// Выводить дату элемента
                            "DISPLAY_NAME" => "Y",	// Выводить название элемента
                            "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
                            "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                            "FIELD_CODE" => array(	// Поля
                                0 => "",
                                1 => "",
                            ),
                            "FILTER_NAME" => "",	// Фильтр
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                            "IBLOCK_ID" => "4",	// Код информационного блока
                            "IBLOCK_TYPE" => "information_materials",	// Тип информационного блока (используется только для проверки)
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                            "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                            "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                            "NEWS_COUNT" => "5",	// Количество новостей на странице
                            "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                            "PAGER_TITLE" => "",	// Название категорий
                            "PARENT_SECTION" => "",	// ID раздела
                            "PARENT_SECTION_CODE" => "",	// Код раздела
                            "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                            "PROPERTY_CODE" => array(	// Свойства
                                0 => "ADDIT_CLASS",
                                1 => "",
                            ),
                            "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
                            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                            "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
                            "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
                            "SET_STATUS_404" => "N",	// Устанавливать статус 404
                            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                            "SHOW_404" => "N",	// Показ специальной страницы
                            "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
                            "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                            "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
                            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                        ),
                        false
                    );?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:catalog.section.list",
                        "g-lab_produce-list",
                        array(
                            "ADD_SECTIONS_CHAIN" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "COUNT_ELEMENTS" => "Y",
                            "IBLOCK_ID" => "1",
                            "IBLOCK_TYPE" => "catalog",
                            "SECTION_CODE" => "",
                            "SECTION_FIELDS" => array(
                                0 => "",
                                1 => "",
                            ),
                            "SECTION_ID" => READY_PROD_SECTION_ID,
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array(
                                0 => "",
                                1 => "",
                            ),
                            "SHOW_PARENT_NAME" => "N",
                            "TOP_DEPTH" => "3",
                            "VIEW_MODE" => "LIST",
                            "COMPONENT_TEMPLATE" => "g-lab_produce-list",
                            "HIDE_SECTION_NAME" => "N"
                        ),
                        false
                    );?>
                    <div class="f-block f-block_home">
                        <div class="container container_form">
                            <div class="f-block__caption">
                                <?$APPLICATION->IncludeFile(
                                    $APPLICATION->GetTemplatePath("include_areas/advantages_caption.php"),
                                    Array(),
                                    Array("MODE"=>"html")
                                );?>
                            </div>
                            <div class="f-block__inner">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:news.list",
                                    "g-lab_advantages",
                                    Array(
                                        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                                        "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                                        "AJAX_MODE" => "N",	// Включить режим AJAX
                                        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                                        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                                        "BLOCK_TITLE" => "",	// Заголовок блока
                                        "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                                        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                                        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                                        "CACHE_TYPE" => "A",	// Тип кеширования
                                        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                                        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                                        "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                                        "DISPLAY_DATE" => "N",	// Выводить дату элемента
                                        "DISPLAY_NAME" => "Y",	// Выводить название элемента
                                        "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
                                        "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                                        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                                        "FIELD_CODE" => array(	// Поля
                                            0 => "",
                                            1 => "",
                                        ),
                                        "FILTER_NAME" => "",	// Фильтр
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                                        "IBLOCK_ID" => "6",	// Код информационного блока
                                        "IBLOCK_TYPE" => "information_materials",	// Тип информационного блока (используется только для проверки)
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                                        "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                                        "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                                        "NEWS_COUNT" => "8",	// Количество новостей на странице
                                        "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                                        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                                        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                                        "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                                        "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                                        "PAGER_TITLE" => "",	// Название категорий
                                        "PARENT_SECTION" => "",	// ID раздела
                                        "PARENT_SECTION_CODE" => "",	// Код раздела
                                        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                                        "PROPERTY_CODE" => array(	// Свойства
                                            0 => "ADDIT_CLASS",
                                            1 => "",
                                        ),
                                        "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
                                        "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                                        "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
                                        "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
                                        "SET_STATUS_404" => "N",	// Устанавливать статус 404
                                        "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                                        "SHOW_404" => "N",	// Показ специальной страницы
                                        "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
                                        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                                        "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
                                        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                                    ),
                                    false
                                );?>
                                <form method="post" action="#" class="f-ordering">
                                    <div class="f-ordering__column f-ordering__hide">
                                        <p class="f-ordering__row">
                                            <label for="f-ordering__type" class="f-ordering__label">Тип короба</label>
                                    <span class="f-ordering__select-wrap">
                                        <select name="type" id="f-ordering__type" class="f-ordering__select">
                                            <option value="4-х клапанный">4-х клапанный</option>
                                            <option value="4-х клапанный">2-х клапанный</option>
                                            <option value="4-х клапанный">5-х клапанный</option>
                                        </select>
                                    </span>
                                        </p>
                                        <p class="f-ordering__row">
                                            <label class="f-ordering__label">Размеры (Д - Ш - В)</label>
                                    <span class="f-ordering__row-inner">
                                        <input type="text" name="length" class="f-ordering__input f-ordering__input_small">
                                        <input type="text" name="width" class="f-ordering__input f-ordering__input_small">
                                        <input type="text" name="height" class="f-ordering__input f-ordering__input_small">
                                    </span>
                                        </p>
                                        <p class="f-ordering__row">
                                            <label for="mark_name" class="f-ordering__label">Марка картона</label>
                                    <span class="f-ordering__row-inner">
                                        <input type="text" name="mark_name" id="mark_name"
                                               class="f-ordering__input f-ordering__input_middle">
                                        <span class="f-ordering__select-wrap f-ordering__select-wrap_middle">
                                            <select name="mark_type" class="f-ordering__select">
                                                <option value="Бурый">Бурый</option>
                                                <option value="Серый">Серый</option>
                                                <option value="Белый">Белый</option>
                                            </select>
                                        </span>
                                    </span>
                                        </p>
                                        <p class="f-ordering__row">
                                            <label for="f-ordering__profile" class="f-ordering__label">Профиль картона</label>
                                    <span class="f-ordering__select-wrap">
                                        <select name="profile" id="f-ordering__profile" class="f-ordering__select">
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                        </select>
                                    </span>
                                        </p>
                                        <p class="f-ordering__row">
                                            <label for="f-ordering__print" class="f-ordering__label">Печать</label>
                                    <span class="f-ordering__select-wrap">
                                        <select name="print" id="f-ordering__print" class="f-ordering__select">
                                            <option value="yes">Да</option>
                                            <option value="no">Нет</option>
                                        </select>
                                    </span>
                                        </p>
                                        <p class="f-ordering__row">
                                            <label for="f-ordering__color" class="f-ordering__label">Количество цветов</label>
                                            <input type="text" name="color" id="f-ordering__color" class="f-ordering__input">
                                        </p>
                                        <p class="f-ordering__row">
                                            <label for="f-ordering__square" class="f-ordering__label">Площадь запечатки</label>
                                            <input type="text" name="square" id="f-ordering__square" class="f-ordering__input">
                                        </p>
                                    </div>
                                    <div class="f-ordering__column">
                                        <p class="f-ordering__row f-ordering__hide">
                                            <label for="f-ordering__circulation" class="f-ordering__label">Тираж</label>
                                            <input type="text" name="circulation" id="f-ordering__circulation"
                                                   class="f-ordering__input">
                                        </p>
                                        <p class="f-ordering__row f-ordering__hide">
                                            <label for="f-ordering__comment" class="f-ordering__label">Комментарий</label>
                                            <input type="text" name="comment" id="f-ordering__comment" class="f-ordering__input">
                                        </p>
                                        <p class="f-ordering__row">
                                            <label for="f-ordering__org" class="f-ordering__label">Организация</label>
                                            <input type="text" name="org" id="f-ordering__org" class="f-ordering__input">
                                        </p>
                                        <p class="f-ordering__row">
                                            <label for="f-ordering__contact" class="f-ordering__label">Контактное лицо</label>
                                            <input type="text" name="contact" required id="f-ordering__contact"
                                                   class="f-ordering__input">
                                        </p>
                                        <p class="f-ordering__row f-ordering__hide">
                                            <label for="f-ordering__email" class="f-ordering__label">E-mail</label>
                                            <input type="email" name="email" required id="f-ordering__email"
                                                   class="f-ordering__input">
                                        </p>
                                        <p class="f-ordering__row">
                                            <label for="f-ordering__phone" class="f-ordering__label">Телефон</label>
                                            <input type="text" name="phone" placeholder="+7 (___)-___-__-__" id="f-ordering__phone"
                                                   class="phone-mask f-ordering__input">
                                        </p>
                                        <p class="f-ordering__row f-ordering__row_submit">
                                            <button class="f-ordering__submit"><span
                                                    class="f-ordering__submit-text">отправить</span></button>
                                        </p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "g-lab_news-list-main",
                        array(
                            "ACTIVE_DATE_FORMAT" => "j F Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "BLOCK_TITLE" => "Новости",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "FILTER_NAME" => "",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => "2",
                            "IBLOCK_TYPE" => "information_materials",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "2",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "420",
                            "PROPERTY_CODE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "SET_BROWSER_TITLE" => "N",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER1" => "DESC",
                            "SORT_ORDER2" => "ASC",
                            "COMPONENT_TEMPLATE" => "g-lab_news-list-main"
                        ),
                        false
                    );?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "g-lab_articles-list-main",
                        array(
                            "ACTIVE_DATE_FORMAT" => "j F Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "BLOCK_TITLE" => "Полезные <span class=\"h-materials__caption h-materials__caption_b\">материалы</span>",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "FILTER_NAME" => "",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => "3",
                            "IBLOCK_TYPE" => "information_materials",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "5",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "300",
                            "PROPERTY_CODE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "SET_BROWSER_TITLE" => "N",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER1" => "DESC",
                            "SORT_ORDER2" => "ASC",
                            "COMPONENT_TEMPLATE" => "g-lab_articles-list-main"
                        ),
                        false
                    );?>
                <?}*/?>                
                <?if($bIsNewsDetailPage || $bIsArticleDetailPage || $bIsCatalogListPage || $bIsCatalogDetailPage){?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "g-lab_breadcrumbs",
                        Array(
                            "PATH" => "",
                            "SITE_ID" => "s1",
                            "START_FROM" => "0"
                        )
                    );?>
                <?}?>
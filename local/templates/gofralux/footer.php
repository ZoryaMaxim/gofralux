<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
            </main>
            <footer class="footer">
                <div class="footer__top">
                    <div class="container container_contact">
                        <div class="footer__left">
                            <nav class="b-menu">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "g-lab_bottom",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "1",
                                        "MENU_CACHE_GET_VARS" => array(""),
                                        "MENU_CACHE_TIME" => "3600",
                                        "MENU_CACHE_TYPE" => "N",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "ROOT_MENU_TYPE" => "bottom_1",
                                        "USE_EXT" => "Y"
                                    )
                                );?>
                            </nav>
                            <nav class="b-menu">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "g-lab_bottom",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "1",
                                        "MENU_CACHE_GET_VARS" => array(""),
                                        "MENU_CACHE_TIME" => "3600",
                                        "MENU_CACHE_TYPE" => "N",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "ROOT_MENU_TYPE" => "bottom_2",
                                        "USE_EXT" => "Y"
                                    )
                                );?>
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "g-lab_bottom",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "1",
                                        "MENU_CACHE_GET_VARS" => array(""),
                                        "MENU_CACHE_TIME" => "3600",
                                        "MENU_CACHE_TYPE" => "N",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "ROOT_MENU_TYPE" => "bottom_3",
                                        "USE_EXT" => "Y"
                                    )
                                );?>
                            </nav>
                        </div>
                        <div class="footer__right">
                            <div class="f-contact">
                                <p class="f-contact__caption">
                                    <a href="/contacts/" class="b-menu__link b-menu__link_caption">
                                        <span class="f-contact__caption-contact">Контакты</span> Москва
                                    </a>
                                </p>
                                <div class="f-contact__data">
                                    <div class="text-block__small_rd">
                                        <?$APPLICATION->IncludeFile(
                                            $APPLICATION->GetTemplatePath("include_areas/footer_contacts_moscow.php"),
                                            Array(),
                                            Array("MODE"=>"html")
                                        );?>
                                    </div>
                                </div>
                            </div>
                            <div class="f-contact">
                                <p class="f-contact__caption">
                                    <a href="/contacts/contacts-vladimir.php" class="b-menu__link b-menu__link_caption">
                                        <span class="f-contact__caption-contact">Контакты</span> Владимир
                                    </a>
                                </p>
                                <div class="f-contact__data">
                                    <?$APPLICATION->IncludeFile(
                                        $APPLICATION->GetTemplatePath("include_areas/footer_contacts_vladimir.php"),
                                        Array(),
                                        Array("MODE"=>"html")
                                    );?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer__bottom">
                    <div class="container container_footer">
                        <div class="copyright">
                            © 2011–<?=date('Y')?> <?$APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath("include_areas/footer_copyright.php"),
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                        </div>
                        <div class="g-lab<?if($bIsContactsPage){?> g-lab_show<?}?>">
                            <a target="_blank" href="https://www.g-lab.ru/" class="g-lab__link">Разработка сайта G-lab</a>
                        </div>
                    </div>
                </div>        
            </footer>
        </div>
        <div id="price" class="modal">
            <a href="#close" class="modal__close-area"></a>
            <div class="modal__container">
                <a href="#close" class="modal__close m-form__close"></a>
                <?$APPLICATION->IncludeComponent(
                    "g-lab:order.quick",
                    "order-form",
                    Array(
                        "COMPONENT_TEMPLATE" => "order-form",
                        "EMAIL_TO" => "gp@g-lab.ru",
                        "EVENT_MESSAGE_ID" => array(0=>"9",),
                        "OK_TEXT" => "Благодарим за заявку, мы обязательно с Вами свяжемся в ближайшее время",
                        "AJAX_MODE" => "Y",  // режим AJAX
                        "AJAX_OPTION_SHADOW" => "N", // затемнять область
                        "AJAX_OPTION_JUMP" => "N", // скроллить страницу до компонента
                        "AJAX_OPTION_STYLE" => "N", // подключать стили
                        "AJAX_OPTION_HISTORY" => "N",
                    )
                );?>
            </div>
        </div>
        <div id="thankyou" class="modal">
            <a href="#close" class="modal__close-area"></a>
            <div class="modal__container">
                <a href="#close" class="modal__close m-form__close"></a>
                <form action="#" method="post" class="m-form">
                    <div class="m-form__container">
                        <p class="m-form__caption">Спасибо</p>
                        <p class="m-form__text">Ваша заявка отправлена.</p>
                        <p class="m-form__text">Мы свяжемся с Вами в ближайшее время.</p>
        
                    </div>
                </form>
            </div>
        </div>        
        <script defer src="<?=SITE_DIR?>assets/dist/js/app.min.js"></script>
    </body>
</html>
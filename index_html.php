<!doctype html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/dist/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffcc33">
    <meta name="theme-color" content="#ffcc33">
    <title>Гофралюкс</title>
    <script>
        (function () {
            if (sessionStorage.foutFontsLoaded) {
                document.documentElement.className += " fonts-loaded";
                return;
            }
            <?php include dirname(__FILE__) . '/assets/src/js/fontfaceobserver.standalone.js'; ?>
            <?php include dirname(__FILE__) . '/assets/dist/js/fonts.min.js'; ?>
        })();
    </script>
    <style><?php include dirname(__FILE__) . '/assets/dist/css/head.min.css'; ?></style>
</head>
<body>
<div id="wrapper" class="wrapper">
    <main class="main">
        <div class="main__header main__header_home">
            <header class="header">
                <div class="header__top header__top_home">
                    <div class="container container_top">
                        <div class="burger">
                            <div class="burger__btn"><span></span></div>
                        </div>
                        <nav class="top-menu top-menu_home">
                            <ul class="top-menu__list">
                                <li class="top-menu__item top-menu__item_top top-menu__item_expand top-menu__item_home"><a
                                            class="top-menu__link"
                                            href="#">Каталог
                                        продукции</a>
                                    <ul class="top-menu__sub top-menu__sub_show">
                                        <li class="top-menu__sub-item top-menu__sub-item_caption top-menu__sub-item_ready">
                                            <a
                                                    href="#" class="top-menu__sub-link top-menu__sub-link_caption">готовые
                                                изделия</a>
                                            <ul class="third-menu third-menu_ready">
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Гофротара
                                                        для...</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">4-х
                                                        клапанные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Самосборные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="/sub-catalog.php" class="third-menu__link">Короба
                                                        ГОСТ</a>
                                                </li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Крупноформатные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Многоточечная
                                                        склейка</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Комплектующие
                                                        к
                                                        гофрокоробам</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Прочие
                                                        изделия</a>
                                                </li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Гофрокартон
                                                        в
                                                        листах</a></li>
                                            </ul>
                                        </li>
                                        <li class="top-menu__sub-item  top-menu__sub-item_caption  top-menu__sub-item_service">
                                            <a
                                                    href="#" class="top-menu__sub-link top-menu__sub-link_caption">Дополнительные
                                                услуги</a>
                                            <ul class="third-menu">
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Разработка
                                                        конструкций</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        штанц-форм</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        печатных форм</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        образцов</a></li>
                                            </ul>
                                        </li>
                                        <li class="top-menu__sub-item top-menu__sub-item_blank">
                                            <ul class="menu-buttons">
                                                <li class="menu-buttons__item"><a
                                                            class="menu-buttons__link menu-buttons__link_fefco"
                                                            href="#"><span class="menu-buttons__icon"></span><span
                                                                class="menu-buttons__text">каталог fefco</span></a>
                                                </li>
                                                <li class="menu-buttons__item"><a
                                                            class="menu-buttons__link menu-buttons__link_gost"
                                                            href="#"><span class="menu-buttons__icon"></span><span
                                                                class="menu-buttons__text">Каталог гост</span></a></li>
                                            </ul>
                                        </li>
                                    </ul>

                                </li>
                                <li class="top-menu__item top-menu__item_top"><a class="top-menu__link" href="/catalog.php">Производство</a>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_offset"><a
                                            class="top-menu__link"
                                            href="/online.php">Онлайн заказ</a>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_parent"><a
                                            class="top-menu__link"
                                            href="#">О компании</a>
                                    <ul class="top-menu__sub">
                                        <li class="top-menu__sub-item"><a href="/news.php"
                                                                          class="top-menu__sub-link">Новости</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/materials.php" class="top-menu__sub-link">Полезные
                                                материалы</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/details.php"
                                                                          class="top-menu__sub-link">Реквизиты</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_parent"><a class="top-menu__link"
                                                                                 href="#">Контакты</a>
                                    <ul class="top-menu__sub top-menu__sub_right">
                                        <li class="top-menu__sub-item"><a href="/moscow.php"
                                                                          class="top-menu__sub-link">Контакты Москва</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/vladimir.php" class="top-menu__sub-link">Контакты Владимир</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="m-menu-buttons">
                                <li class="m-menu-buttons__item"><a
                                            class="m-menu-buttons__link m-menu-buttons__link_fefco"
                                            href="#">каталог
                                        fefco</a>
                                </li>
                                <li class="m-menu-buttons__item"><a
                                            class="m-menu-buttons__link m-menu-buttons__link_gost"
                                            href="#">Каталог гост</a></li>
                            </ul>
                        </nav>
                        <div class="logo"><a href="/" class="logo__link"></a></div>
                        <ul class="top-phone">
                            <li class="top-phone__item"><a href="tel:+74956004612" class="top-phone__link">+7 (495) 600 46 12</a></li>
                            <li class="top-phone__item"><a href="tel:+74956004613" class="top-phone__link">+7 (495) 600 46 13</a></li>
                        </ul>
                    </div>
                </div>
                <div class="container container_head">
                    <form class="head-phone">
                        <div class="head-phone__inner">
                            <label for="head-phone__input" class="head-phone__label">Закажите звонок</label>
                            <input placeholder="+7 (___)-___-__-__" type="text" name="phone" id="head-phone__input"
                                   class="head-phone__input phone-mask">
                        </div>
                        <button class="head-phone__button"></button>
                    </form>
                    <div class="head-phone head-phone_mail">
                        <div class="head-phone__inner head-phone__inner_mail">
                            <a href="mailto:gofralux@mail.ru" class="head-phone__mail">gofralux@mail.ru</a>
                        </div>
                        <a href="mailto:gofralux@mail.ru" class="head-phone__button head-phone__button_mail"></a>
                    </div>
                    <?php if(false): ?>
                    <div class="head-phone head-phone_mail">
                        <a href="mailto:gofralux@mail.ru" class="head-phone__button head-phone__button_mail"></a>
                    </div>
                    <?php endif; ?>
                    <a href="tel:+74956004612" class="m-head-phone m-head-phone_home"></a>
                    <a href="mailto:gofralux@mail.ru" class="m-head-phone m-head-phone_mail m-head-phone_home"></a>
                </div>
            </header>
            <div class="container container_video">
                <div class="text-block">
                    <p class="text-block__caption">Производство всех видов <br><span
                                class="text-block__caption_bold">гофротары</span> <span
                                class="text-block__caption_small">для вашего бизнеса</span></p>
                    <p class="text-block__normal">
                        Оперативное изготовление любой гофротары
                    </p>
                    <p class="text-block__small">
                        <span class="text-block__small_st">Индивидуальный подход</span>
                        <span class="text-block__small_nd">Ответственное исполнение</span>
                        <span class="text-block__small_rd">Собственное производство</span>
                    </p>
                    <span class="text-block__scroll"></span>
                </div>
            </div>
            <div class="video">
                <video autoplay loop muted src="/assets/dist/video/gofralux.mp4" class="video__item"></video>
            </div>
        </div>
        <div class="container">
            <div class="h-production">
                <p class="h-production__title">Наше <b
                            class="h-production__title h-production__title_b">производство</b></p>
                <div class="h-production__list">
                    <div class="h-production__item">
                        <figure class="h-production__image">
                            <img class="h-production__img" src="/assets/dist/img/h-man1.jpg"
                                 alt="Линия производства 4-х клапанных коробов">
                            <figcaption class="h-production__caption"><span class="h-production__caption-text"><span
                                            class="h-production__caption-text h-production__caption-text_line">Линия производства </span>4-х клапанных коробов</span>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="h-production__item">
                        <figure class="h-production__image">
                            <img class="h-production__img" src="/assets/dist/img/h-man2.jpg"
                                 alt="Линия производства основы для гофрокартона">
                            <figcaption class="h-production__caption"><span class="h-production__caption-text"><span
                                            class="h-production__caption-text h-production__caption-text_line">Линия производства </span>основы для гофрокартона</span>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="h-production__desc">Принцип восприятия заполняет катарсис. Отсюда естественно следует, что
                    конфликт может быть получен из опыта. Исчисление предикатов, как следует из вышесказанного,
                    рефлектирует непредвиденный дуализм, учитывая опасность, которую представляли собой писания Дюринга
                    для не окрепшего еще немецкого рабочего движения. Принцип восприятия заполняет катарсис.
                    Отсюда естественно следует, что конфликт может быть получен из опыта.
                    Отсюда естественно следует, что конфликт может быть получен из опыта. Исчисление предикатов, как
                    следует из вышесказанного, рефлектирует непредвиденный дуализм, учитывая опасность, которую
                    представляли собой писания Дюринга для не окрепшего еще немецкого рабочего движения.
                </div>
                <a href="/catalog.php" class="more-link more-link_production"><span class="more-link__text">Узнать больше</span></a>
            </div>
        </div>
        <div class="stages">
            <div class="container container_stages">
                <p class="stages__title">этапы<span class="stages__title_b"> производственного процесса</span></p>
                <ul class="stages__list">
                    <li class="stages__item">
                        <div class="stages__item-text">Разработка конструкции гофротары</div>
                    </li>
                    <li class="stages__item stages__item_stamp">
                        <div class="stages__item-text">Изготовление штампов для сложных изделий</div>
                    </li>
                    <li class="stages__item stages__item_form">
                        <div class="stages__item-text">Заказ печатной формы <br>(1-4 цвета)</div>
                    </li>
                    <li class="stages__item stages__item_box">
                        <div class="stages__item-text">Изготовление гофротары (нанесение печати)</div>
                    </li>
                    <li class="stages__item stages__item_delivery">
                        <div class="stages__item-text">Доставка в любую точку России</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="produce">
                <div class="produce__caption">
                    мы<span class="produce__caption produce__caption_b"> производим</span>
                </div>
                <div class="produce__list">
                    <div class="produce__item">
                        <div class="produce__inner produce__inner_long">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim1.png" alt="Гофротара для..." class="produce__img">
                                    <figcaption class="produce__title">Гофротара для...</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">пищевых продуктов</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">мебели</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">кондитерских изделий</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">пиццы</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">конфет</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">печенья</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">бутылок на заказ</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гофрокороба архивные и папки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Яиц</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">овощей</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim8.png" alt="4-х клапанные гофрокороба" class="produce__img">
                                    <figcaption class="produce__title">4-х клапанные гофрокороба</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Короба для алкогольной продукции</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Короба для пищевой продукции</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Для продуктов и бытовой химии</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner produce__inner_long">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim4.png" alt="самосборные гофрокороба" class="produce__img">
                                    <figcaption class="produce__title">самосборные гофрокороба</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Архивные короба и папки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Овощные лотки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Лотки для птицы</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Лотки для рыбы</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Лотки для кондитерских изделий</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Короба для пиццы</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Короба для обуви</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гофрокороба с ручкой</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гофрокороба сложной высечки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">шоубоксы</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">лотки для мяса</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim1.png" alt="короба гост" class="produce__img">
                                    <figcaption class="produce__title">короба гост</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гост №18 630х320х240</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гост №38 380х304х285</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гост №17 380х285х225</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гост №7 380х253х237</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гост №21 380х380х228</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure produce__figure_empty">
                                    <img src="assets/dist/img/anim5.png" alt="Крупноформатные гофрокороба" class="produce__img">
                                    <figcaption class="produce__title">Крупноформатные гофрокороба</figcaption>
                                </figure>
                            </a>

                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure produce__figure_empty">
                                    <img src="assets/dist/img/anim1.png" alt="Многоточечная склейка" class="produce__img">
                                    <figcaption class="produce__title">Многоточечная склейка</figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim2.png" alt="комплектующие к гофрокоробам" class="produce__img">
                                    <figcaption class="produce__title">комплектующие к гофрокоробам</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гофропрокладки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гофровкладыши</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Обечайки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Решетки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Ложементы</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim7.png" alt="прочие изделия" class="produce__img">
                                    <figcaption class="produce__title">прочие изделия</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Уголки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Круги</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner produce__inner_right">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim4.png" alt="гофрокартон в листах" class="produce__img">
                                    <figcaption class="produce__title">гофрокартон в листах</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub produce__sub_right">
                                <li class="produce__sub-item produce__sub-item_right"><a href="#" class="produce__sub-link">Микрогофрокартон</a></li>
                                <li class="produce__sub-item produce__sub-item_right"><a href="#" class="produce__sub-link">Тлехслойный гофрокартон</a></li>
                                <li class="produce__sub-item produce__sub-item_right"><a href="#" class="produce__sub-link">Пятислойный гофрокартон</a></li>
                                <li class="produce__sub-item produce__sub-item_right"><a href="#" class="produce__sub-link">Беленый гофрокартон</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim6.png" alt="Дополнительные услуги" class="produce__img">
                                    <figcaption class="produce__title">Дополнительные услуги</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="#" class="produce__sub-link">Разработка конструкций</a></li>
                                <li class="produce__sub-item"><a href="#" class="produce__sub-link">Изготовление штанц-форм</a></li>
                                <li class="produce__sub-item"><a href="#" class="produce__sub-link">Изготовление печатных форм</a></li>
                                <li class="produce__sub-item"><a href="#" class="produce__sub-link">Изготовление образцов</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="f-block f-block_home">
            <div class="container container_form">
                <p class="f-block__caption">Не нашли подходящей гофротары?<span class="f-block__caption_b">Сделаем на заказ!</span>
                </p>
                <div class="f-block__inner">
                    <ul class="advantages">
                        <li class="advantages__item"><span class="advantages__text">Собственное производство</span>
                        </li>
                        <li class="advantages__item advantages__item_clock"><span class="advantages__text">15 лет на рынке гофроупаковки</span>
                        </li>
                        <li class="advantages__item advantages__item_map"><span class="advantages__text">4 линии производства гофрокартона</span>
                        </li>
                        <li class="advantages__item advantages__item_cmyk"><span class="advantages__text">Современное полноцветное <br>флексопечатное оборудование (CMYK)</span>
                        </li>
                        <li class="advantages__item advantages__item_truck"><span class="advantages__text">Оперативная доставка гофротары, <br>собственным транспортом</span>
                        </li>
                        <li class="advantages__item advantages__item_deadline"><span class="advantages__text">Сроки выполнения заказа от 2-х дней</span>
                        </li>
                    </ul>
                    <form method="post" action="#" class="f-ordering">
                        <div class="f-ordering__column f-ordering__hide">
                            <p class="f-ordering__row">
                                <label for="f-ordering__type" class="f-ordering__label">Тип короба</label>
                                <span class="f-ordering__select-wrap">
                                    <select name="type" id="f-ordering__type" class="f-ordering__select">
                                        <option value="4-х клапанный">4-х клапанный</option>
                                        <option value="4-х клапанный">2-х клапанный</option>
                                        <option value="4-х клапанный">5-х клапанный</option>
                                    </select>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label class="f-ordering__label">Размеры (Д - Ш - В)</label>
                                <span class="f-ordering__row-inner">
                                    <input type="text" name="length" class="f-ordering__input f-ordering__input_small">
                                    <input type="text" name="width" class="f-ordering__input f-ordering__input_small">
                                    <input type="text" name="height" class="f-ordering__input f-ordering__input_small">
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="mark_name" class="f-ordering__label">Марка картона</label>
                                <span class="f-ordering__row-inner">
                                    <input type="text" name="mark_name" id="mark_name"
                                           class="f-ordering__input f-ordering__input_middle">
                                    <span class="f-ordering__select-wrap f-ordering__select-wrap_middle">
                                        <select name="mark_type" class="f-ordering__select">
                                            <option value="Бурый">Бурый</option>
                                            <option value="Серый">Серый</option>
                                            <option value="Белый">Белый</option>
                                        </select>
                                    </span>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__profile" class="f-ordering__label">Профиль картона</label>
                                <span class="f-ordering__select-wrap">
                                    <select name="profile" id="f-ordering__profile" class="f-ordering__select">
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                    </select>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__print" class="f-ordering__label">Печать</label>
                                <span class="f-ordering__select-wrap">
                                    <select name="print" id="f-ordering__print" class="f-ordering__select">
                                            <option value="yes">Да</option>
                                            <option value="no">Нет</option>
                                    </select>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__color" class="f-ordering__label">Количество цветов</label>
                                <input type="text" name="color" id="f-ordering__color" class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__square" class="f-ordering__label">Площадь запечатки</label>
                                <input type="text" name="square" id="f-ordering__square" class="f-ordering__input">
                            </p>
                        </div>
                        <div class="f-ordering__column">
                            <p class="f-ordering__row f-ordering__hide">
                                <label for="f-ordering__circulation" class="f-ordering__label">Тираж</label>
                                <input type="text" name="circulation" id="f-ordering__circulation"
                                       class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row f-ordering__hide">
                                <label for="f-ordering__comment" class="f-ordering__label">Комментарий</label>
                                <input type="text" name="comment" id="f-ordering__comment" class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__org" class="f-ordering__label">Организация</label>
                                <input type="text" name="org" id="f-ordering__org" class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__contact" class="f-ordering__label">Контактное лицо</label>
                                <input type="text" name="contact" required id="f-ordering__contact"
                                       class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row f-ordering__hide">
                                <label for="f-ordering__email" class="f-ordering__label">E-mail</label>
                                <input type="email" name="email" required id="f-ordering__email"
                                       class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__phone" class="f-ordering__label">Телефон</label>
                                <input type="text" name="phone" placeholder="+7 (___)-___-__-__" id="f-ordering__phone"
                                       class="phone-mask f-ordering__input">
                            </p>
                            <p class="f-ordering__row f-ordering__row_submit">
                                <button class="f-ordering__submit"><span
                                            class="f-ordering__submit-text">отправить</span></button>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="h-news">
                <p class="h-news__caption">Новости</p>
                <div class="h-news__list">
                    <div class="h-news__item tilter">
                        <figure class="h-news__figure tilter__figure">
                            <a href="#" class="h-news__figure-wrap"><img src="/assets/dist/img/news1.jpg"
                                                                         alt="У нас новые часы работы!"
                                                                         class="h-news__img tilter__deco--overlay"></a>
                            <figcaption class="h-news__title tilter__caption"><a class="h-news__link" href="#">У нас
                                    новые часы
                                    работы!</a></figcaption>
                        </figure>
                        <div class="h-news__content ">
                            <time datetime="2017-01-25" class="h-news__date">25 января</time>
                            <div class="h-news__desc">Принцип восприятия заполняет катарсис. Отсюда естественно следует,
                                то конфликт может быть получен из опыта. Исчисление предикатов, как следует из
                                вышесказанного, рефлектирует непредвиденный дуализм, учитывая опасность, которую
                                представляли собой писания Дюринга для не окрепшего еще немецкого рабочего движения.
                                Принцип восприятия заполняет катарсис. Отсюда естественно следует, что конфликт
                                может быть получен из опыта.
                            </div>
                        </div>
                    </div>
                    <div class="h-news__item tilter">
                        <figure class="h-news__figure tilter__figure">
                            <a href="" class="h-news__figure-wrap "><img src="/assets/dist/img/news2.jpg"
                                                                        alt="У нас новые часы работы!"
                                                                        class="tilter__deco--overlay h-news__img"></a>
                            <figcaption class="h-news__title tilter__caption"><a class="h-news__link" href="#">у нас
                                    очень много
                                    новостей, Все для вас, любимые клиенты</a></figcaption>
                        </figure>
                        <div class="h-news__content ">
                            <time datetime="2017-01-25" class="h-news__date">25 января</time>
                            <div class="h-news__desc">Принцип восприятия заполняет катарсис. Отсюда естественно следует,
                                то конфликт может быть получен из опыта. Исчисление предикатов, как следует из
                                вышесказанного, рефлектирует непредвиденный дуализм, учитывая опасность, которую
                                представляли собой писания Дюринга для не окрепшего еще немецкого рабочего движения.
                                Принцип восприятия заполняет катарсис. Отсюда естественно следует, что конфликт
                                может быть получен из опыта.
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/news.php" class="more-link more-link_news"><span class="more-link__text">Все новости</span></a>
            </div>
        </div>
        <div class="h-materials h-materials_home">
            <div class="container">
                <div class="h-materials__caption">Полезные<span class="h-materials__caption h-materials__caption_b">материалы</span>
                </div>
                <div class="h-materials__list">
                    <div class="h-materials__item tilter">
                        <figure class="h-materials__figure tilter__figure"><a href="#" class="h-materials__figure-wrap"><img
                                        src="assets/dist/img/material.jpg" alt="Появился  новый
                                    вид товара" class="h-materials__img tilter__deco--overlay"></a>
                            <figcaption class="h-materials__title tilter__caption"><a href="#" class="h-materials__link">Появился новый
                                    вид товара</a></figcaption>
                        </figure>
                        <div class="h-materials__content ">Принцип восприятия заполняет катарсис.
                            Отсюда естественно следует, что конфликт ожет быть получен из опыта. Отсюда
                            естественно следует, что конфликт может быть получен из опыта. Исчисление предикатов, как
                            следует из вышесказанного, рефлектирует непредвиденный дуализм, учитывая опасность...
                        </div>
                    </div>
                    <div class="h-materials__item tilter">
                        <figure class="h-materials__figure tilter__figure"><a href="#" class="h-materials__figure-wrap"><img
                                        src="assets/dist/img/material.jpg" alt="Появился  новый
                                    вид товара" class="h-materials__img tilter__deco--overlay"></a>
                            <figcaption class="h-materials__title tilter__caption"><a href="#" class="h-materials__link">Появился новый
                                    вид товара</a></figcaption>
                        </figure>
                        <div class="h-materials__content ">Принцип восприятия заполняет катарсис.
                            Отсюда естественно следует, что конфликт ожет быть получен из опыта. Отсюда
                            естественно следует, что конфликт может быть получен из опыта. Исчисление предикатов, как
                            следует из вышесказанного, рефлектирует непредвиденный дуализм, учитывая опасность...
                        </div>
                    </div>
                    <div class="h-materials__item tilter">
                        <figure class="h-materials__figure tilter__figure"><a href="#" class="h-materials__figure-wrap "><img
                                        src="assets/dist/img/material.jpg" alt="Появился  новый
                                    вид товара" class="h-materials__img tilter__deco--overlay"></a>
                            <figcaption class="h-materials__title tilter__caption"><a href="#" class="h-materials__link">Появился новый
                                    вид товара</a></figcaption>
                        </figure>
                        <div class="h-materials__content ">Принцип восприятия заполняет катарсис.
                            Отсюда естественно следует, что конфликт ожет быть получен из опыта. Отсюда
                            естественно следует, что конфликт может быть получен из опыта. Исчисление предикатов, как
                            следует из вышесказанного, рефлектирует непредвиденный дуализм, учитывая опасность...
                        </div>
                    </div>
                </div>
                <a href="/materials.php" class="more-link more-link_materials"><span class="more-link__text">Все полезные материалы</span></a>
            </div>
        </div>
    </main>
<?php require_once __DIR__.'/footer.php'?>
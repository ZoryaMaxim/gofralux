<!doctype html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/dist/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffcc33">
    <meta name="theme-color" content="#ffcc33">
    <title>Гофроящик №18 (стандартный по ГОСТу 13513-86) 630х320х340 мм</title>
    <script>
        (function () {
            if (sessionStorage.foutFontsLoaded) {
                document.documentElement.className += " fonts-loaded";
                return;
            }
            <?php include dirname(__FILE__) . '/assets/src/js/fontfaceobserver.standalone.js'; ?>
            <?php include dirname(__FILE__) . '/assets/dist/js/fonts.min.js'; ?>
        })();
    </script>
    <style><?php include dirname(__FILE__) . '/assets/dist/css/head.min.css'; ?></style>
</head>
<body>
<div id="wrapper" class="wrapper">
    <main class="main">
        <div class="main__header">
            <header class="header">
                <div class="header__top">
                    <div class="container container_top">
                        <div class="burger">
                            <div class="burger__btn"><span></span></div>
                        </div>
                        <nav class="top-menu">
                            <ul class="top-menu__list">
                                <li class="top-menu__item top-menu__item_top top-menu__item_expand"><a
                                            class="top-menu__link"
                                            href="#">Каталог
                                        продукции</a>
                                    <ul class="top-menu__sub">
                                        <li class="top-menu__sub-item top-menu__sub-item_caption top-menu__sub-item_ready">
                                            <a
                                                    href="#" class="top-menu__sub-link top-menu__sub-link_caption">готовые
                                                изделия</a>
                                            <ul class="third-menu third-menu_ready">
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Гофротара
                                                        для...</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">4-х
                                                        клапанные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Самосборные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Короба
                                                        ГОСТ</a>
                                                </li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Крупноформатные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Многоточечная
                                                        склейка</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Комплектующие
                                                        к
                                                        гофрокоробам</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Прочие
                                                        изделия</a>
                                                </li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Гофрокартон
                                                        в
                                                        листах</a></li>
                                            </ul>
                                        </li>
                                        <li class="top-menu__sub-item  top-menu__sub-item_caption  top-menu__sub-item_service">
                                            <a
                                                    href="#" class="top-menu__sub-link top-menu__sub-link_caption">Дополнительные
                                                услуги</a>
                                            <ul class="third-menu">
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Разработка
                                                        конструкций</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        штанц-форм</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        печатных форм</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        образцов</a></li>
                                            </ul>
                                        </li>
                                        <li class="top-menu__sub-item top-menu__sub-item_blank">
                                            <ul class="menu-buttons">
                                                <li class="menu-buttons__item"><a
                                                            class="menu-buttons__link menu-buttons__link_fefco"
                                                            href="#"><span class="menu-buttons__icon"></span><span
                                                                class="menu-buttons__text">каталог fefco</span></a>
                                                </li>
                                                <li class="menu-buttons__item"><a
                                                            class="menu-buttons__link menu-buttons__link_gost"
                                                            href="#"><span class="menu-buttons__icon"></span><span
                                                                class="menu-buttons__text">Каталог гост</span></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="top-menu__item top-menu__item_top"><a class="top-menu__link"
                                                                                 href="/catalog.php">Производство</a>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_offset"><a
                                            class="top-menu__link"
                                            href="/online.php">Онлайн заказ</a>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_parent"><a
                                            class="top-menu__link"
                                            href="#">О компании</a>
                                    <ul class="top-menu__sub">
                                        <li class="top-menu__sub-item"><a href="/news.php"
                                                                          class="top-menu__sub-link">Новости</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/materials.php"
                                                                          class="top-menu__sub-link">Полезные
                                                материалы</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/details.php"
                                                                          class="top-menu__sub-link">Реквизиты</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_parent"><a
                                            class="top-menu__link"
                                            href="#">Контакты</a>
                                    <ul class="top-menu__sub top-menu__sub_right">
                                        <li class="top-menu__sub-item"><a href="/moscow.php"
                                                                          class="top-menu__sub-link">Контакты Москва</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/vladimir.php"
                                                                          class="top-menu__sub-link">Контакты
                                                Владимир</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="m-menu-buttons">
                                <li class="m-menu-buttons__item"><a
                                            class="m-menu-buttons__link m-menu-buttons__link_fefco"
                                            href="#">каталог
                                        fefco</a>
                                </li>
                                <li class="m-menu-buttons__item"><a
                                            class="m-menu-buttons__link m-menu-buttons__link_gost"
                                            href="#">Каталог гост</a></li>
                            </ul>
                        </nav>
                        <div class="logo"><a href="/" class="logo__link"></a></div>
                        <ul class="top-phone">
                            <li class="top-phone__item"><a href="tel:+74956004612" class="top-phone__link">+7 (495) 600
                                    46 12</a></li>
                            <li class="top-phone__item"><a href="tel:+74956004613" class="top-phone__link">+7 (495) 600
                                    46 13</a></li>
                        </ul>
                    </div>
                </div>
                <div class="container container_head">
                    <form class="head-phone">
                        <div class="head-phone__inner">
                            <label for="head-phone__input" class="head-phone__label">Закажите звонок</label>
                            <input placeholder="+7 (___)-___-__-__" type="text" name="phone" id="head-phone__input"
                                   class="head-phone__input phone-mask">
                        </div>
                        <button class="head-phone__button"></button>
                    </form>
                    <div class="head-phone head-phone_mail">
                        <a href="mailto:gofralux@mail.ru" class="head-phone__button head-phone__button_mail"></a>
                    </div>
                    <a href="tel:+74956004612" class="m-head-phone"></a>
                    <a href="mailto:gofralux@mail.ru" class="m-head-phone m-head-phone_mail"></a>
                </div>
            </header>
        </div>
        <div class="breadcrumbs">
            <div class="container">
                <ul class="breadcrumbs__list">
                    <li class="breadcrumbs__item"><a class="breadcrumbs__crumb" href="/">Главная</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_delimeter">/</li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__crumb" href="/catalog.php">Каталог
                            продукции</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_delimeter">/</li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__crumb" href="/sub-catalog.php">Короба ГОСТ</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_delimeter">/</li>
                    <li class="breadcrumbs__item"><span class="breadcrumbs__crumb breadcrumbs__crumb_last">Гост №18 630х320х240</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <h1 class="h1 h1_product"><span class="h1__b">Гофроящик №18</span> (стандартный по ГОСТу 13513-86)
                630х320х340 мм</h1>
            <div class="product">
                <div class="product__item">
                    <ul class="product__thumbnails">
                        <li data-src="/assets/dist/img/bigproduct.jpg" class="product__thumbnails-item product__thumbnails-item_main"><a href="#" class="product__thumbnails-link"><img src="/assets/dist/img/bigproduct.jpg"
                                                                                                             alt="product"
                                                                                                             class="product__thumbnails-img product__thumbnails-img_main"></a>
                        </li>
                        <li data-src="/assets/dist/img/bigproduct.jpg" class="product__thumbnails-item"><a href="#" class="product__thumbnails-link"><img src="/assets/dist/img/product.jpg"
                                                                                                             alt="product"
                                                                                                             class="product__thumbnails-img"></a>
                        </li>
                        <li data-src="/assets/dist/img/bigproduct.jpg" class="product__thumbnails-item"><a href="#" class="product__thumbnails-link"><img src="/assets/dist/img/product.jpg"
                                                                                                             alt="product"
                                                                                                             class="product__thumbnails-img"></a>
                        </li>
                        <li data-src="/assets/dist/img/bigproduct.jpg" class="product__thumbnails-item"><a href="#" class="product__thumbnails-link"><img
                                        src="/assets/dist/img/product.jpg"
                                        alt="product"
                                        class="product__thumbnails-img"></a>
                        </li>
                    </ul>
                    <a href="#" class="more-link more-link_product modal-btn"><span class="more-link__text more-link__text_product">Запрос расчета цены</span></a>
                </div>
                <article class="product__desc">
                    <b>Описание</b><br>
                    <p>Гофроящик № 18 – это картонная коробка, предназначенная в частности для складирования куриных яиц. Гофроящик № 18 проходит по ГОСТ 13513-86 с внутренними габаритными размерами 630х320х340 мм. Гофроящик 18 в «стандартном» исполнении изготавливается из трехслойного гофрокартона. Гофроящик 18 представляет собой четырехклапанную коробку.<br>
                        Благодаря своим размерам (средняя картонная коробка) стала универсальным упаковочным изделием для хранения и перевозки, всевозможных вещей и предметов.<br>
                        Также данная коробка очень хорошо подходит для переезда. Предназначение (для хранения и перевозки): одежды, посуды, документов, личных вещей, малой оргтехники, комнатные небольшие цветы.</p>
                    <b>Технические характеристики:</b><br>
                    <p>
                        Тип гофроящика - четырехклапанная коробка<br>
                        Стандарт по ГОСТу 13513-86 - № 18<br>
                        Применение - для упаковки (складирования) куриных яиц, упакованных в индивидуальную картонную или пластиковую упаковку (бугорчатую прокладку)<br>
                        Внутренние размеры (длина/ширина/высота) - 630х320х340 мм<br>
                        Материал (стандартное изготовление) - гофрокартон марки Т-23 профиль "В"<br>
                        Цвет наружного слоя целлюлозы (стандартное изготовление) - бурый<br>
                        Печать (стандартное изготовление) - нет<br>
                        Упаковка (стандартное изготовление) - без упаковки (пачки по 20 шт., стянутые стриплентой)
                    </p>
                    <p>
                        Дополнительно рекомендуется*: "примеры вариантов изготовления"<br>
                        Для вентиляции - вырубка четырех отверстий круглой формы с диаметром 20 мм<br>
                        Для погрузо-разгрузочных работ - прорубные ручки для удобства во время переноски заполненных коробок
                    </p>

                </article>
            </div>
        </div>
        <div class="f-block">
            <div class="container container_form">
                <p class="f-block__caption">Не нашли подходящей гофротары?<span class="f-block__caption_b">Сделаем на заказ!</span>
                </p>
                <div class="f-block__inner">
                    <ul class="advantages">
                        <li class="advantages__item"><span class="advantages__text">Собственное производство во <br>Владимирской области</span>
                        </li>
                        <li class="advantages__item advantages__item_clock"><span class="advantages__text">15 лет на рынке гофроупаковки</span>
                        </li>
                        <li class="advantages__item advantages__item_map"><span class="advantages__text">4 линии производства гофрокартона</span>
                        </li>
                        <li class="advantages__item advantages__item_cmyk"><span class="advantages__text">Современное полноцветное <br>флексопечатное оборудование (CMYK)</span>
                        </li>
                        <li class="advantages__item advantages__item_truck"><span class="advantages__text">Оперативная доставка гофротары, <br>собственным транспортом</span>
                        </li>
                        <li class="advantages__item advantages__item_deadline"><span class="advantages__text">Сроки выполнения заказа от 2-х дней</span>
                        </li>
                    </ul>
                    <form method="post" action="#" class="f-ordering">
                        <div class="f-ordering__column f-ordering__hide">
                            <p class="f-ordering__row">
                                <label for="f-ordering__type" class="f-ordering__label">Тип короба</label>
                                <span class="f-ordering__select-wrap">
                                    <select name="type" id="f-ordering__type" class="f-ordering__select">
                                        <option value="4-х клапанный">4-х клапанный</option>
                                        <option value="4-х клапанный">2-х клапанный</option>
                                        <option value="4-х клапанный">5-х клапанный</option>
                                    </select>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label class="f-ordering__label">Размеры (Д - Ш - В)</label>
                                <span class="f-ordering__row-inner">
                                    <input type="text" name="length" class="f-ordering__input f-ordering__input_small">
                                    <input type="text" name="width" class="f-ordering__input f-ordering__input_small">
                                    <input type="text" name="height" class="f-ordering__input f-ordering__input_small">
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="mark_name" class="f-ordering__label">Марка картона</label>
                                <span class="f-ordering__row-inner">
                                    <input type="text" name="mark_name" id="mark_name"
                                           class="f-ordering__input f-ordering__input_middle">
                                    <span class="f-ordering__select-wrap f-ordering__select-wrap_middle">
                                        <select name="mark_type" class="f-ordering__select">
                                            <option value="Бурый">Бурый</option>
                                            <option value="Серый">Серый</option>
                                            <option value="Белый">Белый</option>
                                        </select>
                                    </span>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__profile" class="f-ordering__label">Профиль картона</label>
                                <span class="f-ordering__select-wrap">
                                    <select name="profile" id="f-ordering__profile" class="f-ordering__select">
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                    </select>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__print" class="f-ordering__label">Печать</label>
                                <span class="f-ordering__select-wrap">
                                    <select name="print" id="f-ordering__print" class="f-ordering__select">
                                            <option value="yes">Да</option>
                                            <option value="no">Нет</option>
                                    </select>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__color" class="f-ordering__label">Количество цветов</label>
                                <input type="text" name="color" id="f-ordering__color" class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__square" class="f-ordering__label">Площадь запечатки</label>
                                <input type="text" name="square" id="f-ordering__square" class="f-ordering__input">
                            </p>
                        </div>
                        <div class="f-ordering__column">
                            <p class="f-ordering__row f-ordering__hide">
                                <label for="f-ordering__circulation" class="f-ordering__label">Тираж</label>
                                <input type="text" name="circulation" id="f-ordering__circulation"
                                       class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row f-ordering__hide">
                                <label for="f-ordering__comment" class="f-ordering__label">Комментарий</label>
                                <input type="text" name="comment" id="f-ordering__comment" class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__org" class="f-ordering__label">Организация</label>
                                <input type="text" name="org" id="f-ordering__org" class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__contact" class="f-ordering__label">Контактное лицо</label>
                                <input type="text" name="contact" required id="f-ordering__contact"
                                       class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row f-ordering__hide">
                                <label for="f-ordering__email" class="f-ordering__label">E-mail</label>
                                <input type="email" name="email" required id="f-ordering__email"
                                       class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__phone" class="f-ordering__label">Телефон</label>
                                <input type="text" name="phone" placeholder="+7 (___)-___-__-__" id="f-ordering__phone"
                                       class="phone-mask f-ordering__input">
                            </p>
                            <p class="f-ordering__row f-ordering__row_submit">
                                <button class="f-ordering__submit"><span
                                            class="f-ordering__submit-text">отправить</span></button>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
<?php require_once __DIR__ . '/footer.php' ?>
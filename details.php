<!doctype html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/dist/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffcc33">
    <meta name="theme-color" content="#ffcc33">
    <title>Гофралюкс - Реквизиты</title>
    <script>
        (function () {
            if (sessionStorage.foutFontsLoaded) {
                document.documentElement.className += " fonts-loaded";
                return;
            }
            <?php include dirname(__FILE__) . '/assets/src/js/fontfaceobserver.standalone.js'; ?>
            <?php include dirname(__FILE__) . '/assets/dist/js/fonts.min.js'; ?>
        })();
    </script>
    <style><?php include dirname(__FILE__) . '/assets/dist/css/head.min.css'; ?></style>
</head>
<body>
<div id="wrapper" class="wrapper">
    <main class="main">
        <div class="main__header">
            <header class="header">
                <div class="header__top">
                    <div class="container container_top">
                        <div class="burger">
                            <div class="burger__btn"><span></span></div>
                        </div>
                        <nav class="top-menu">
                            <ul class="top-menu__list">
                                <li class="top-menu__item top-menu__item_top top-menu__item_expand"><a
                                        class="top-menu__link"
                                        href="#">Каталог
                                        продукции</a>
                                    <ul class="top-menu__sub">
                                        <li class="top-menu__sub-item top-menu__sub-item_caption top-menu__sub-item_ready">
                                            <a
                                                href="#" class="top-menu__sub-link top-menu__sub-link_caption">готовые
                                                изделия</a>
                                            <ul class="third-menu third-menu_ready">
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Гофротара
                                                        для...</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">4-х
                                                        клапанные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Самосборные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Короба
                                                        ГОСТ</a>
                                                </li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Крупноформатные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Многоточечная
                                                        склейка</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Комплектующие
                                                        к
                                                        гофрокоробам</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Прочие
                                                        изделия</a>
                                                </li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Гофрокартон
                                                        в
                                                        листах</a></li>
                                            </ul>
                                        </li>
                                        <li class="top-menu__sub-item  top-menu__sub-item_caption  top-menu__sub-item_service">
                                            <a
                                                href="#" class="top-menu__sub-link top-menu__sub-link_caption">Дополнительные
                                                услуги</a>
                                            <ul class="third-menu">
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Разработка
                                                        конструкций</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        штанц-форм</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        печатных форм</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        образцов</a></li>
                                            </ul>
                                        </li>
                                        <li class="top-menu__sub-item top-menu__sub-item_blank">
                                            <ul class="menu-buttons">
                                                <li class="menu-buttons__item"><a
                                                        class="menu-buttons__link menu-buttons__link_fefco"
                                                        href="#"><span class="menu-buttons__icon"></span><span
                                                            class="menu-buttons__text">каталог fefco</span></a>
                                                </li>
                                                <li class="menu-buttons__item"><a
                                                        class="menu-buttons__link menu-buttons__link_gost"
                                                        href="#"><span class="menu-buttons__icon"></span><span
                                                            class="menu-buttons__text">Каталог гост</span></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="top-menu__item top-menu__item_top"><a class="top-menu__link" href="/catalog.php">Производство</a>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_offset"><a
                                        class="top-menu__link"
                                        href="/online.php">Онлайн заказ</a>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_parent"><a
                                        class="top-menu__link"
                                        href="#">О компании</a>
                                    <ul class="top-menu__sub">
                                        <li class="top-menu__sub-item"><a href="/news.php"
                                                                          class="top-menu__sub-link">Новости</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/materials.php" class="top-menu__sub-link">Полезные
                                                материалы</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/details.php"
                                                                          class="top-menu__sub-link">Реквизиты</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_parent"><a class="top-menu__link"
                                                                                                       href="#">Контакты</a>
                                    <ul class="top-menu__sub">
                                        <li class="top-menu__sub-item"><a href="/moscow.php"
                                                                          class="top-menu__sub-link">Контакты Москва</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/vladimir.php" class="top-menu__sub-link">Контакты Владимир</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="m-menu-buttons">
                                <li class="m-menu-buttons__item"><a
                                        class="m-menu-buttons__link m-menu-buttons__link_fefco"
                                        href="#">каталог
                                        fefco</a>
                                </li>
                                <li class="m-menu-buttons__item"><a
                                        class="m-menu-buttons__link m-menu-buttons__link_gost"
                                        href="#">Каталог гост</a></li>
                            </ul>
                        </nav>
                        <div class="logo"><a href="/" class="logo__link"></a></div>
                        <ul class="top-phone">
                            <li class="top-phone__item"><a href="tel:+74956004612" class="top-phone__link">+7 (495) 600 46 12</a></li>
                            <li class="top-phone__item"><a href="tel:+74956004613" class="top-phone__link">+7 (495) 600 46 13</a></li>
                        </ul>
                    </div>
                </div>
                <div class="container container_head">
                    <form class="head-phone">
                        <div class="head-phone__inner">
                            <label for="head-phone__input" class="head-phone__label">Закажите звонок</label>
                            <input placeholder="+7 (___)-___-__-__" type="text" name="phone" id="head-phone__input"
                                   class="head-phone__input phone-mask">
                        </div>
                        <button class="head-phone__button"></button>
                    </form>
                    <div class="head-phone head-phone_mail">
                        <a href="mailto:gofralux@mail.ru" class="head-phone__button head-phone__button_mail"></a>
                    </div>
                    <a href="tel:+74956004612" class="m-head-phone m-head-phone_home"></a>
                    <a href="mailto:gofralux@mail.ru" class="m-head-phone m-head-phone_mail m-head-phone_home"></a>
                </div>
            </header>
        </div>
        <div class="container">
            <article class="details-content">
                <h1 class="h1"><span class="h1__b">Реквизиты</span></h1>
                <p>
                    Полное наименование - ООО «Торговый дом "Гофралюкс"»<br>
<!--                    Регион - Москва<br>-->
                    Юридический адрес - 129090, г. Москва, Большая Спасская дом 8 помещение 51<br>
                    Почтовый адрес - 129090, г. Москва, Большая Спасская дом 8 помещение 51
                </p>
                <p>Идентификационный номер налогоплательщика (ИНН/КПП) - 7727780892/770801001 <br>
                    ОКПО - 09866729<br>
                    ОГРН - 1127746456320<br>
                </p>
                <p>
                    Расчетный счет № - 40702810237000000019<br>
                    Корреспондентский счет № - 30101810100000000716<br>
                    Наименование учреждения банка - ВТБ 24 (ПАО)<br>
                    БИК - 044525716
                </p>
                <p>Генеральный директор - Лаврентьев Владимир Витальевич</p>
                <p>Главный бухгалтер - Чернышева Ирина Александровна</p>
 <!--               <p>
                    Руководство <br>
                    Должность руководителя - Генеральный директор <br>
                    ФИО руководителя - Лаврентьев Владимир Витальевич <br>
                </p>
                <p>Телефон - +7-(920)-910-3111</p>
                <p>
                    Учредители (3)<br>
                    Физические лица (3)<br>
                    Леонид Владимирович Крупницкий<br>
                    Доля: 4 000,00<br>
                    ИНН: 504101798041<br>
                    Владимир Витальевич Лаврентьев<br>
                    Доля: 3 000,00<br>
                    ИНН: 771815395902<br>
                    Игорь Васильевич Лосев<br>
                    Доля: 3 000,00<br>
                    ИНН: 504101700151<br>
                </p>-->
                <a href="#" class="more-link more-link_details"><span class="more-link__text">скачать реквизиты</span></a>
            </article>
        </div>
<?php require_once __DIR__.'/footer.php'?>
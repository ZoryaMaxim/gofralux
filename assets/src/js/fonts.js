var mainFont = new FontFaceObserver('ProximaNova');

Promise.all([mainFont.load()]).then(function () {
    document.documentElement.className += " fonts-loaded";
    sessionStorage.foutFontsLoaded = true;
});
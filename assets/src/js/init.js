/*(function ($) {
 $(document).ready(function () {

 });
 })(jQuery);*/

$(document).ready(function () {
    addPhoneMask($);
});


document.addEventListener("DOMContentLoaded", function (event) {
    localStorage.clear();
    loadSVG();
    moreSub();
    clearPlaceholder();
    burger();
    newsAnimation();
    /*sendFormOrder();*/
    scrollHeader();
    gallery();
    tableWrap();
    loadMap();
    modalOrder();
    modalClose();
    moreItems();
    validateForm();
    resizeInit();
    /*
     loadStyle();
     mainSlider();
     itemSlider();


     resizeInit();
     coaches();
     loadMap();
     videoYoutube();
     gallery();*/
});

window.onload = function () {
    /*    document.querySelector('html').classList.add('load');
     document.querySelector('.preloader').classList.add('hide');*/
};

function resizeInit() {
    window.addEventListener("resize", function () {
        moreSub();
    });
}

function modalOrder() {
    var mdlBtn = document.querySelectorAll('.modal-btn'),
        modal = document.querySelector('#price');
    if (mdlBtn) {
        [].slice.call(mdlBtn).forEach(function (el, pos) {
            el.addEventListener('click', function (e) {
                e.preventDefault();
                var caption = modal.querySelector('.m-form__caption'),
                    hiddenName = modal.querySelector('.m-form__product-name'),
                    hiddenPrice = modal.querySelector('.m-form__product-price'),
                    totalRow = modal.querySelector('.m-form__row_hide'),
                    priceText = modal.querySelector('.m-form__sub_hide'),
                    quantity = modal.querySelector('.m-form__input_number'),
                    sub = modal.querySelector('.m-form__sub_change');

                modal.querySelector('.m-form__product-id').value = el.parentNode.dataset.id;

                if (el.classList.contains('modal-btn_fast')) {
                    var price = el.parentNode.querySelector('.goods__price').textContent.replace(',', '.'),
                        total = (parseInt(quantity.value) * parseFloat(price)).toFixed(2) + ' р.',
                        totalInput = modal.querySelector('.m-form__product-total'),
                        totalText = modal.querySelector('.m-form__total-input');

                    caption.textContent = 'состав быстрого заказа';
                    sub.textContent = el.parentNode.querySelector('.goods__link').textContent;
                    hiddenName.value = el.parentNode.querySelector('.goods__link').textContent;

                    hiddenName.disabled = false;

                    priceText.classList.add('show');
                    priceText.querySelector('.m-form__sub_price').textContent = price;
                    hiddenPrice.value = price;
                    hiddenPrice.disabled = false;

                    totalRow.classList.add('show');
                    /**/
                    quantityCalc();
                    /**/
                }
                else {
                    sub.textContent = document.querySelector('.h1').textContent;
                    hiddenName.value = document.querySelector('.h1').textContent;
                }

                modal.classList.add('in');
            });
        });
    }
}

function quantityCalc() {
    var modal = document.querySelector('#price'),
        quantity = modal.querySelector('.m-form__input_number'),
        totalInput = modal.querySelector('.m-form__product-total'),
        totalText = modal.querySelector('.m-form__total-input'),
        price = modal.querySelector('.m-form__product-price').value,
        total = (parseInt(quantity.value) * parseFloat(price)).toFixed(2) + ' р.';


    totalInput.disabled = false;
    totalInput.value = total;
    totalText.textContent = total;

    quantity.addEventListener('change', function (e) {
        var quantityVal = parseInt(this.value);

        total = (quantityVal * parseFloat(price)).toFixed(2) + ' р.';

        totalText.textContent = total;
        totalInput.value = total;
    });
    quantity.addEventListener('keyup', function (e) {
        var quantityVal = parseInt(this.value);

        if (quantityVal < 1 || isNaN(quantityVal)) {
            quantityVal = 1;
        }

        total = (quantityVal * parseFloat(price)).toFixed(2) + ' р.';

        totalText.textContent = total;
        totalInput.value = total;
    });

}

function modalAnswer(data) {
    var container = document.querySelector('.m-form__answer'),

        form = document.querySelector('#price .m-form');
    container.innerHTML = data;
    form.classList.add('answer');
    var btn = document.querySelector('#price .more-link_error');
    if (btn) {
        btn.addEventListener('click', function (e) {
            e.preventDefault();
            form.classList.remove('answer');
        });
    }
}

function modalClose() {
    var close = document.querySelectorAll('.modal__close, .modal__close-area'),
        modal = document.querySelectorAll('.modal');
    if (close) {
        [].slice.call(close).forEach(function (el, pos) {
            el.addEventListener('click', function (e) {
                e.preventDefault();
                window.location.hash = '';
                [].slice.call(modal).forEach(function (el, pos) {
                    el.classList.remove('in');
                    el.querySelector('.m-form').classList.remove('answer');
                });
            });
        });
    }
}

function formAnswer(data) {
    var modal = document.getElementById('thankyou');
    modal.querySelector('.m-form__container').innerHTML = data;
    modal.classList.add('in');
}

function validateForm() {
    var forms = document.querySelectorAll('form.head-phone, .f-ordering, #price .m-form');
    [].slice.call(forms).forEach(function (form, pos) {
        form.querySelector('button').addEventListener('click', function (e) {
            var req = form.querySelectorAll('input:required');
            [].slice.call(req).forEach(function (el, pos) {
                if (el.value == "") {
                    el.classList.add('validate-error');
                    el.addEventListener('keyup', function () {
                       if(this.value != ""){
                           el.classList.remove('validate-error');
                       }
                    });
                } else {
                    el.classList.remove('validate-error');
                }
            });
        });
    });


}

function moreSub() {
    var content = document.querySelector('.catalog-content');

    if (content) {
        var btn = content.querySelector('.more-link'),
            hide = content.querySelector('.catalog-content__desc'),
            min = outerHeight(hide) * 0.3,
            max = outerHeight(hide);

        if (max < 230) {
            btn.classList.add('hide');
            hide.classList.remove('more');
        } else {
            hide.classList.add('more');
            btn.classList.remove('hide');
            hide.style.maxHeight = min + 'px';
        }

        hide.dataset.min = min + 'px';
        hide.dataset.max = max + 'px';


        if (!content.classList.contains('calc')) {
            content.classList.add('calc');
            btn.addEventListener('click', function (e) {
                e.preventDefault();
                var text = this.querySelector('.more-link__text');

                if (!hide.classList.contains('show')) {
                    hide.style.maxHeight = hide.dataset.max;
                    hide.classList.add('show');
                } else {
                    hide.style.maxHeight = hide.dataset.min;
                    hide.classList.remove('show');
                }


                if (!text.classList.contains('show')) {
                    text.dataset.text = this.textContent;
                    text.classList.add('show');
                    text.textContent = 'Свернуть';
                } else {
                    text.textContent = text.dataset.text;
                    text.classList.remove('show');
                }
            });
        }

    }
}

function moreItems() {
    var el = document.querySelector('.h-materials_inner, .h-news_inner');


    if (el) {
        var btn = el.querySelector('.more-link'),
            hide = el.querySelector('.h-news__list_hide, .h-materials__list_hide');

        if (!hide.classList.contains('calc')) {
            hide.classList.remove('hide');
            hide.style.maxHeight = outerHeight(hide) + 'px';
            hide.classList.add('calc');
            hide.classList.add('hide');
        }

        btn.addEventListener('click', function (e) {
            e.preventDefault();
            var text = this.querySelector('.more-link__text');

            hide.classList.toggle('hide');

            if (!text.classList.contains('show')) {
                text.dataset.text = this.textContent;
                text.classList.add('show');
                text.textContent = 'Свернуть';
            } else {
                text.textContent = text.dataset.text;
                text.classList.remove('show');
            }
        });
    }
}

function scrollHeader() {
    var w = document.documentElement.clientWidth,
        scrl = window.scrollY,
        oldSc = window.scrollY,
        menu = document.querySelector('.header__top'),
        mItem = menu.querySelector('.top-menu__item_expand'),
        sMenu = mItem.querySelector('.top-menu__sub'),
        tgl = (w >= 1020) ? 800 : 530;

    if (scrl >= tgl) {
        if (w >= 1020) {
            mItem.classList.remove('top-menu__item_home');
            sMenu.classList.remove('top-menu__sub_show');
        }
        setTimeout(function () {
            menu.classList.add('scroll');
        }, 300);
    } else {
        menu.classList.remove('scroll');
        if (menu.classList.contains('header__top_home')) {
            mItem.classList.add('top-menu__item_home');
            sMenu.classList.add('top-menu__sub_show');
        }
    }
    window.addEventListener('scroll', function () {
        scrl = window.scrollY;
        w = document.documentElement.clientWidth;
        tgl = (w >= 1020) ? 800 : 530;

        if (scrl >= tgl) {


            if (w >= 1020) {
                mItem.classList.remove('top-menu__item_home');
                sMenu.classList.remove('top-menu__sub_show');
            }
            setTimeout(function () {
                menu.classList.add('scroll');
            }, 300);

        } else {
            menu.classList.remove('scroll');
            if (menu.classList.contains('header__top_home')) {
                mItem.classList.add('top-menu__item_home');
                sMenu.classList.add('top-menu__sub_show');
            }
        }
    });
}

function clearPlaceholder() {
    var txt = null,
        inputs = document.querySelectorAll('input, textarea');
    if (inputs) {
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('focus', function () {
                txt = this.placeholder;
                this.placeholder = '';
            });
            inputs[i].addEventListener('blur', function () {
                this.placeholder = txt;
            });
        }
    }
}

function burger() {
    var b = document.querySelector('.burger'),
        menu = document.querySelector('.top-menu'),
        logo = document.querySelector('.logo'),
        expand = document.querySelector('.top-menu__item_expand > .top-menu__link'),
        ready = document.querySelector('.third-menu'),
        tM = [].slice.call(document.querySelectorAll('.top-menu__item_parent > .top-menu__link')),
        sMenu = expand.parentNode.parentNode.querySelector('.top-menu__sub'),
        header = document.querySelector('.header__top');
    if (b) {
        b.addEventListener('click', function (e) {
            e.preventDefault();
            this.classList.toggle('in');
            this.lastElementChild.classList.toggle('in');
            menu.classList.toggle('in');
            header.classList.toggle('in');
            logo.classList.toggle('in');
        });
        if (expand) {
            expand.addEventListener('click', function (e) {
                e.preventDefault();
                var w = document.documentElement.clientWidth;


                if (w >= 1020) {
                    this.parentNode.classList.toggle('top-menu__item_home');
                    sMenu.classList.toggle('top-menu__sub_show');

                } else {
                    this.parentNode.classList.toggle('in');
                    ready.classList.toggle('in');
                    if (this.parentNode.classList.contains('in')) {
                        sMenu.classList.add('top-menu__sub_show');
                    } else {
                        sMenu.classList.remove('top-menu__sub_show');
                    }
                }
            });
        }
        tM.forEach(function (el, pos) {
            el.addEventListener('click', function (e) {
                e.preventDefault();
                el.parentNode.classList.toggle('in');
                el.parentNode.querySelector('.top-menu__sub').classList.toggle('in');
            });
        });
    }
}

function outerHeight(el) {
    var height = el.offsetHeight;
    var style = getComputedStyle(el);
    height += parseInt(style.marginTop) + parseInt(style.marginBottom);
    return height;
}

function loadMap() {
    var map = document.querySelector('.map__map');
    if (map) {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "///api-maps.yandex.ru/2.1/?lang=ru_RU";
        script.defer = true;
        //IE triggers this event when the file is loaded

        if (script.attachEvent) {
            script.attachEvent('onreadystatechange', function () {
                if (script.readyState == 'complete' || script.readyState == 'loaded')
                    setMap();
            });
        }

        //Other browsers trigger this one
        if (script.addEventListener) script.addEventListener('load', setMap, false);

        document.body.appendChild(script);
    }
}

function setMap() {
    if (typeof ymaps !== 'undefined') {
        ymaps.ready(function () {
            ymaps.ready(init);
        });
    }
}

function init() {
    ymaps.ready(function () {
        var objct = document.querySelector('.map__map'),
            data = objct.dataset,
            map = new ymaps.Map(objct, {
                center: [data.lat, data.lng],
                zoom: data.zoom,
                controls: ["smallMapDefaultSet"]
            }, {
                searchControlProvider: "yandex#search"
            }),
            c = new ymaps.Placemark([data.lat, data.lng], {}, {

                /*iconLayout: 'default#image',

                 iconImageHref: '/wp-content/themes/football/assets/dist/images/mark.png',

                 iconImageSize: [44, 70],

                 iconImageOffset: [-22, -70],*/
            });
        map.behaviors.disable("scrollZoom");
        map.geoObjects.add(c);
    })
}

function loadSVG() {
    var sprire = document.createElement('div'),
        body = document.body || document.getElementsByTagName('body')[0],
        el = document.querySelector('.wrapper'),
        svg = localStorage.getItem("spriteSvg"),
        path = '/assets/dist/svg/sprite.svg';
    if (svg) {
        sprire.innerHTML = svg;
        sprire.classList.add('sprite-svg');
        /*if (style.styleSheet) {
         style.styleSheet.cssText = css;
         } else {
         style.appendChild(document.createTextNode(css));
         }*/
    } else {
        axios.get(path)
            .then(function (response) {
                svg = response.data;
                localStorage.setItem("spriteSvg", svg)
                sprire.innerHTML = svg;
                sprire.classList.add('sprite-svg');
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    body.insertBefore(sprire, el);
}

function gallery() {
    var gallery = document.querySelector('.product__thumbnails_img');

    if (gallery) {
        lightGallery(gallery, {
            thumbnail: true,
        });
    }
}

function tableWrap() {
    var table = document.querySelectorAll('table');
    for (var i = 0, ln = table.length; i < ln; i++) {
        var div = document.createElement('div');
        div.setAttribute('class', 'table-wrap');
        insertAfter(div, table[i]);
        table[i].nextSibling.appendChild(table[i]);
    }
}

function insertAfter(elem, refElem) {
    return refElem.parentNode.insertBefore(elem, refElem.nextSibling);
}

function addPhoneMask($) {
    $('.phone-mask').mask("+7 (999)-999-99-99");
}

function newsAnimation() {
    [].slice.call(document.querySelectorAll('.tilter')).forEach(function (el, pos) {
        new TiltFx(el, {
            movement: {
                lines: {
                    translation: {x: 40, y: 40, z: 0},
                    reverseAnimation: {duration: 1500, easing: 'easeOutElastic'}
                },
                caption: {
                    translation: {x: 5, y: 5, z: 0},
                    rotation: {x: 0, y: 0, z: 0},
                    reverseAnimation: {duration: 1000, easing: 'easeOutExpo'}
                },
                overlay: {
                    translation: {x: -15, y: -15, z: 0},
                    rotation: {x: 0, y: 0, z: 0},
                    reverseAnimation: {duration: 750, easing: 'easeOutExpo'}
                },
                shine: {
                    translation: {x: 100, y: 100, z: 0},
                    reverseAnimation: {duration: 750, easing: 'easeOutExpo'}
                }
            }
        });
    });
}

function sendFormOrder() {
    var form = document.querySelector('.f-ordering');

    if (form) {
        form.addEventListener('submit', function (e) {
            e.preventDefault();
            var data = encodeURIComponent(toJSONString(form));
            console.log(data);
            axios.post('/', 'data=' + data)
                .then(function (response) {
                    console.log('test');
                })
                .catch(function (error) {
                    console.log(error);
                });
        });
    }

}

function toJSONString(form) {
    var obj = {};
    var elements = form.querySelectorAll("input, select, textarea");
    for (var i = 0; i < elements.length; ++i) {
        var element = elements[i];
        var name = element.name;
        var value = element.value;

        if (name) {
            obj[name] = value;
        }
    }
    return JSON.stringify(obj);
}
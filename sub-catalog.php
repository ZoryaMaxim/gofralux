<!doctype html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/dist/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffcc33">
    <meta name="theme-color" content="#ffcc33">
    <title>Гофралюкс - Короба ГОСТ от производителя в Москве</title>
    <script>
        (function () {
            if (sessionStorage.foutFontsLoaded) {
                document.documentElement.className += " fonts-loaded";
                return;
            }
            <?php include dirname(__FILE__) . '/assets/src/js/fontfaceobserver.standalone.js'; ?>
            <?php include dirname(__FILE__) . '/assets/dist/js/fonts.min.js'; ?>
        })();
    </script>
    <style><?php include dirname(__FILE__) . '/assets/dist/css/head.min.css'; ?></style>
</head>
<body>
<div id="wrapper" class="wrapper">
    <main class="main">
        <div class="main__header">
            <header class="header">
                <div class="header__top">
                    <div class="container container_top">
                        <div class="burger">
                            <div class="burger__btn"><span></span></div>
                        </div>
                        <nav class="top-menu">
                            <ul class="top-menu__list">
                                <li class="top-menu__item top-menu__item_top top-menu__item_expand"><a
                                        class="top-menu__link"
                                        href="#">Каталог
                                        продукции</a>
                                    <ul class="top-menu__sub">
                                        <li class="top-menu__sub-item top-menu__sub-item_caption top-menu__sub-item_ready">
                                            <a
                                                href="#" class="top-menu__sub-link top-menu__sub-link_caption">готовые
                                                изделия</a>
                                            <ul class="third-menu third-menu_ready">
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Гофротара
                                                        для...</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">4-х
                                                        клапанные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Самосборные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Короба
                                                        ГОСТ</a>
                                                </li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Крупноформатные
                                                        гофрокороба</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Многоточечная
                                                        склейка</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Комплектующие
                                                        к
                                                        гофрокоробам</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Прочие
                                                        изделия</a>
                                                </li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Гофрокартон
                                                        в
                                                        листах</a></li>
                                            </ul>
                                        </li>
                                        <li class="top-menu__sub-item  top-menu__sub-item_caption  top-menu__sub-item_service">
                                            <a
                                                href="#" class="top-menu__sub-link top-menu__sub-link_caption">Дополнительные
                                                услуги</a>
                                            <ul class="third-menu">
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Разработка
                                                        конструкций</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        штанц-форм</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        печатных форм</a></li>
                                                <li class="third-menu__item"><a href="#" class="third-menu__link">Изготовление
                                                        образцов</a></li>
                                            </ul>
                                        </li>
                                        <li class="top-menu__sub-item top-menu__sub-item_blank">
                                            <ul class="menu-buttons">
                                                <li class="menu-buttons__item"><a
                                                        class="menu-buttons__link menu-buttons__link_fefco"
                                                        href="#"><span class="menu-buttons__icon"></span><span
                                                            class="menu-buttons__text">каталог fefco</span></a>
                                                </li>
                                                <li class="menu-buttons__item"><a
                                                        class="menu-buttons__link menu-buttons__link_gost"
                                                        href="#"><span class="menu-buttons__icon"></span><span
                                                            class="menu-buttons__text">Каталог гост</span></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="top-menu__item top-menu__item_top"><a class="top-menu__link" href="/catalog.php">Производство</a>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_offset"><a
                                        class="top-menu__link"
                                        href="/online.php">Онлайн заказ</a>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_parent"><a
                                        class="top-menu__link"
                                        href="#">О компании</a>
                                    <ul class="top-menu__sub">
                                        <li class="top-menu__sub-item"><a href="/news.php"
                                                                          class="top-menu__sub-link">Новости</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/materials.php" class="top-menu__sub-link">Полезные
                                                материалы</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/details.php"
                                                                          class="top-menu__sub-link">Реквизиты</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="top-menu__item top-menu__item_top top-menu__item_parent"><a class="top-menu__link"
                                                                                                       href="#">Контакты</a>
                                    <ul class="top-menu__sub top-menu__sub_right">
                                        <li class="top-menu__sub-item"><a href="/moscow.php"
                                                                          class="top-menu__sub-link">Контакты Москва</a>
                                        </li>
                                        <li class="top-menu__sub-item"><a href="/vladimir.php" class="top-menu__sub-link">Контакты Владимир</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="m-menu-buttons">
                                <li class="m-menu-buttons__item"><a
                                        class="m-menu-buttons__link m-menu-buttons__link_fefco"
                                        href="#">каталог
                                        fefco</a>
                                </li>
                                <li class="m-menu-buttons__item"><a
                                        class="m-menu-buttons__link m-menu-buttons__link_gost"
                                        href="#">Каталог гост</a></li>
                            </ul>
                        </nav>
                        <div class="logo"><a href="/" class="logo__link"></a></div>
                        <ul class="top-phone">
                            <li class="top-phone__item"><a href="tel:+74956004612" class="top-phone__link">+7 (495) 600 46 12</a></li>
                            <li class="top-phone__item"><a href="tel:+74956004613" class="top-phone__link">+7 (495) 600 46 13</a></li>
                        </ul>
                    </div>
                </div>
                <div class="container container_head">
                    <form class="head-phone">
                        <div class="head-phone__inner">
                            <label for="head-phone__input" class="head-phone__label">Закажите звонок</label>
                            <input placeholder="+7 (___)-___-__-__" type="text" name="phone" id="head-phone__input"
                                   class="head-phone__input phone-mask">
                        </div>
                        <button class="head-phone__button"></button>
                    </form>
                    <div class="head-phone head-phone_mail">
                        <a href="mailto:gofralux@mail.ru" class="head-phone__button head-phone__button_mail"></a>
                    </div>
                    <a href="tel:+74956004612" class="m-head-phone"></a>
                    <a href="mailto:gofralux@mail.ru" class="m-head-phone m-head-phone_mail"></a>
                </div>
            </header>
        </div>
        <div class="breadcrumbs">
            <div class="container">
                <ul class="breadcrumbs__list">
                    <li class="breadcrumbs__item"><a class="breadcrumbs__crumb" href="/">Главная</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_delimeter">/</li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__crumb" href="/catalog.php">Каталог продукции</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_delimeter">/</li>
                    <li class="breadcrumbs__item"><span class="breadcrumbs__crumb breadcrumbs__crumb_last">Короба ГОСТ</span></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <article class="catalog-content">
                <h1 class="h1"><span class="h1__b">Короба ГОСТ</span> от производителя в москве</h1>
                <div class="catalog-content__desc">
                    Упаковка из гофрокартона ГОСТ: изготовление и поставки в Москве<br>
                    Наша фирма выполняет заказы на гофротару, соответствующую требованиям ГОСТ. Наши клиенты – пищевые предприятия, производственные и торговые фирмы, транспортные и логистические организации. Упаковка из гофрокартона ГОСТ подходит для перевозки и хранения легких и тяжелых грузов, маленьких и больших предметов.<br>
                    Мы поставляем, по выбору заказчика, коробки в сборе или заготовки из гофрокартона, предназначенные для сборки на месте. Второй способ предпочтителен, поскольку плоские заготовки, из которых легко собирается гофротара ГОСТ, экономичны при транспортировке и хранении.<br>
                    Если заказчику негде хранить оптовые партии гофротары, наша фирма предлагает услуги складского хранения купленной тары – ее можно забирать самовывозом, по мере необходимости.
                </div>
                <div class="catalog-content__desc catalog-content__desc_hide hide">
                    <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A architecto consequuntur doloribus
                        incidunt libero numquam qui repellendus repudiandae sunt! Beatae, blanditiis cupiditate
                        distinctio ducimus earum illo non perferendis qui repudiandae.
                    </div>
                    <div>Consequuntur debitis earum ex laborum pariatur quasi quidem sed suscipit vitae. Accusantium cum
                        dolorum earum, eligendi maiores neque officia omnis, pariatur porro quae quas quasi sed sequi
                        suscipit, voluptas voluptatem.
                    </div>
                    <div>Adipisci aliquid amet animi aspernatur blanditiis doloribus dolorum eos et ex hic illum impedit
                        in labore magni nemo perferendis quaerat quo repellat repellendus reprehenderit saepe sapiente
                        sit ut, vel veniam.
                    </div>
                    <div>A ad adipisci aliquam architecto beatae cum dignissimos enim, est eum exercitationem fugiat
                        incidunt laudantium maiores nam nisi numquam perspiciatis possimus quia rerum sapiente sed
                        similique sunt suscipit vero vitae?
                    </div>
                    <div>Aliquam dicta ducimus eius esse nesciunt, placeat sequi veritatis vitae voluptate voluptatibus?
                        Alias, debitis dolorem eligendi enim itaque iure labore libero maxime minus necessitatibus optio
                        repellat saepe sit vel voluptatem!
                    </div>
                    <div>Architecto delectus eius esse illum iure repudiandae vero! A ab accusamus assumenda blanditiis
                        dolore, est excepturi fuga, harum in incidunt iste labore laborum, laudantium molestiae nostrum
                        possimus repellat. Rem, sed!
                    </div>
                    <div>Accusantium aliquid aut deleniti distinctio doloremque dolorum hic labore minima numquam odit
                        placeat porro quisquam, repellendus reprehenderit sequi soluta suscipit unde ut veritatis
                        voluptate. A alias architecto beatae dolor voluptas.
                    </div>
                    <div>At autem blanditiis doloribus eligendi facere impedit libero minima non provident recusandae
                        tenetur vel, velit? Adipisci, aperiam dolore et harum hic in itaque iure molestiae odit
                        perspiciatis recusandae sequi tempora?
                    </div>
                    <div>Ab animi, at blanditiis delectus dicta dolorum eius error esse fugiat labore laudantium nostrum
                        odit perspiciatis placeat similique sit tenetur? Eligendi illum perspiciatis rem similique vero?
                        Impedit qui tempora vel.
                    </div>
                    <div>Aut consectetur consequatur culpa dicta dignissimos dolores dolorum enim excepturi
                        exercitationem explicabo facere illum in ipsam ipsum iure labore magnam maxime modi, omnis
                        possimus recusandae repellat sed sint tempore, voluptates!
                    </div>
                    <div>Asperiores ea error expedita fugit illo iure iusto laborum nostrum nulla numquam pariatur quae
                        quod reiciendis rem saepe velit, vero voluptatem. Accusamus animi cum, dicta dolor praesentium
                        qui reiciendis ullam?
                    </div>
                    <div>Ad alias autem commodi corporis, doloribus iste nemo quae quibusdam rem reprehenderit sed sint
                        voluptate, voluptates! Ad dicta doloremque dolorum excepturi expedita, fugit ipsum itaque maxime
                        molestias odio provident voluptatem?
                    </div>
                    <div>Accusamus architecto commodi, consequatur cumque deleniti dolorem ducimus ea, eius, impedit
                        incidunt inventore iusto molestiae neque nihil nisi optio quam quia quis quod sit sunt ut velit
                        veniam voluptates voluptatum.
                    </div>
                    <div>Alias aliquid animi assumenda cupiditate debitis dicta ea et ex fugit itaque molestias natus
                        neque nulla odio perspiciatis possimus quam quibusdam quisquam quos recusandae repellat sed,
                        temporibus ut veritatis voluptates.
                    </div>
                    <div>Aperiam at distinctio eveniet fuga impedit labore laboriosam laborum minima modi officiis optio
                        quae quo, reprehenderit sapiente veniam veritatis vitae! Corporis ea fuga fugiat iste iure nam
                        provident ratione repellendus?
                    </div>
                    <div>Blanditiis consequatur eaque incidunt, laborum magnam similique veritatis! Autem cum eligendi
                        enim eveniet excepturi id illum incidunt ipsum libero magni maxime modi nesciunt officia,
                        quaerat sapiente suscipit velit vitae voluptatibus!
                    </div>
                    <div>Adipisci, aliquid animi aperiam blanditiis consectetur consequatur culpa debitis dolorum eaque
                        earum fugit in laboriosam magnam maiores molestiae numquam officia quasi quibusdam saepe
                        similique sint ullam unde voluptatum. Aliquam, dolores?
                    </div>
                    <div>Alias, aliquam aut ducimus enim expedita harum nesciunt nisi sint ullam! Corporis cupiditate
                        debitis dolore eos illum magnam molestiae nemo optio quaerat sequi! Fugiat praesentium qui
                        reiciendis ut, voluptas voluptatibus!
                    </div>
                    <div>Aliquid architecto assumenda consectetur consequatur cupiditate deserunt doloribus eligendi eos
                        esse et facilis libero magnam maiores neque non nostrum odit qui, quidem rem sunt tenetur vel
                        voluptatum. Asperiores nobis, veniam.
                    </div>
                    <div>A accusamus eveniet facilis id laudantium possimus quos reprehenderit rerum velit voluptatum.
                        Earum enim est harum illo modi placeat quidem repudiandae veritatis, vitae! Dolore eligendi
                        fugiat magnam modi quidem similique.
                    </div>
                    <div>A aliquid consequuntur corporis cupiditate deserunt earum enim ipsa ipsum laboriosam laudantium
                        minus molestiae odio omnis, possimus provident repellat veniam! Hic id, quos. Amet dolor maiores
                        modi odio, placeat vero.
                    </div>
                    <div>Animi atque, corporis dolorum enim error esse et eveniet impedit labore laborum modi, molestiae
                        omnis quas quidem quod sequi tenetur totam unde veritatis voluptatem? Adipisci architecto cum
                        ipsum magnam qui.
                    </div>
                    <div>Dolorem, eius quam quod recusandae reiciendis voluptas! Aliquam aut debitis expedita fugit id
                        nulla officia optio placeat quae quas quidem, reprehenderit sint! Dignissimos ea impedit nemo,
                        quaerat repudiandae ut voluptatum?
                    </div>
                    <div>Autem error fuga ipsam ipsum nam, natus non vitae voluptates. Accusamus ad aliquam debitis
                        dolorem eligendi et, illum itaque, molestias nulla rem sed sit totam, ullam unde vel? Id, ullam?
                    </div>
                    <div>A, ab est explicabo facere fugit iure labore maiores molestiae nihil odit officiis perspiciatis
                        provident quaerat quos ratione repellat rerum sed, suscipit tempora unde velit vitae,
                        voluptatibus voluptatum? Commodi, molestiae.
                    </div>
                    <div>Ad amet assumenda autem cum debitis dolore error esse expedita inventore ipsam natus nihil,
                        odit pariatur provident quaerat quis repellat similique sint, suscipit unde. Consequuntur fugit
                        nostrum optio possimus ratione?
                    </div>
                    <div>Ad autem debitis ducimus incidunt labore quibusdam repellat repellendus soluta temporibus
                        velit! Fugit, ipsam, maiores. Ab, animi beatae culpa cum ea magnam molestias mollitia natus
                        nulla praesentium suscipit vel veniam?
                    </div>
                    <div>Accusamus alias blanditiis cumque dolore eius, illum laboriosam nemo qui quis saepe sed sequi,
                        tempore totam? Aliquam amet fugiat laborum molestiae officiis praesentium quasi soluta unde
                        voluptate. Pariatur, repellat, similique.
                    </div>
                    <div>A aut debitis, dolorum eius eos esse, fuga harum iure laboriosam nam necessitatibus nisi odit,
                        quas quia quibusdam quisquam quod recusandae reiciendis repellat repellendus saepe sint unde
                        velit! Exercitationem, natus!
                    </div>
                    <div>Aliquid, corporis dolorem doloribus fugit hic iusto laudantium modi numquam, odio provident
                        quidem rem saepe similique sunt tempora tenetur voluptate voluptatibus. Alias cum eius in iste
                        laboriosam laborum libero sint.
                    </div>
                    <div>Accusamus aspernatur beatae, dolore ex ipsam iste necessitatibus nulla perferendis repellendus
                        sapiente velit voluptates? Assumenda debitis dolore, doloremque earum eligendi expedita id iste
                        iusto laborum laudantium officia omnis temporibus unde.
                    </div>
                    <div>Aspernatur ducimus expedita fugit harum ipsa modi officiis perspiciatis possimus provident qui
                        quia quis quod, quos repudiandae rerum sed soluta suscipit totam ut, voluptate. Cumque est iusto
                        omnis tempore voluptate!
                    </div>
                    <div>Facere fugit labore, natus necessitatibus nobis numquam possimus ullam. Architecto, commodi
                        delectus deserunt distinctio esse impedit iure labore, molestias, nam nemo nulla officia
                        officiis repellat rerum tenetur? Ab eligendi, optio!
                    </div>
                    <div>Beatae esse et hic laudantium minus quas qui soluta temporibus. Error facilis fugiat fugit ipsa
                        itaque molestias, nam nihil placeat quidem velit. Accusamus beatae culpa doloribus qui quos sed
                        sunt?
                    </div>
                    <div>Adipisci alias amet, aut consequatur corporis cum dolorem eaque enim expedita in ipsa ipsum
                        iusto magni non nostrum quae quasi quidem quod ratione sequi tempore ullam unde veniam vero
                        voluptate.
                    </div>
                    <div>Consequatur cumque dolore dolorum iste labore molestiae, non obcaecati praesentium quisquam
                        temporibus velit voluptatem? A amet assumenda consequuntur harum incidunt iusto laborum libero
                        maiores mollitia nam omnis quaerat, quas unde!
                    </div>
                    <div>Ad aliquid amet, asperiores cumque, cupiditate debitis dolore facilis nobis repudiandae sit vel
                        vitae! Adipisci, dicta dolore error excepturi expedita facere nobis nulla ullam unde vero.
                        Accusantium deserunt ea fugiat?
                    </div>
                    <div>A accusantium ad amet aperiam asperiores atque consequatur cupiditate distinctio dolore enim
                        hic, inventore labore laboriosam laborum laudantium nostrum odit quas quasi quia quibusdam,
                        ratione repellendus saepe sit vero, voluptatum.
                    </div>
                    <div>Ab at corporis cumque, deleniti dicta expedita iusto maxime pariatur rerum sequi, sint, soluta
                        tempore ullam? Adipisci deleniti excepturi expedita fugiat, id modi necessitatibus nemo nisi
                        pariatur, similique ullam voluptatum?
                    </div>
                    <div>Atque aut autem dolorem ducimus eaque, excepturi fuga molestias repellendus vero vitae? Commodi
                        dolores incidunt iure labore maxime possimus provident quia quibusdam quod, rem repellendus
                        repudiandae similique ullam, vel voluptate!
                    </div>
                    <div>Hic impedit, omnis perspiciatis quam quas reiciendis rerum! A atque culpa doloremque doloribus
                        ducimus eius et eum id iure maxime minima, nemo nihil odio quam quasi quidem sit vel voluptatum.
                    </div>
                    <div>A aperiam blanditiis culpa distinctio est explicabo, hic ipsum laborum maiores molestiae nemo
                        perspiciatis quae quisquam repudiandae sapiente sequi suscipit temporibus ullam unde vero. Illum
                        quisquam repudiandae temporibus velit vitae!
                    </div>
                    <div>Aliquam atque beatae consectetur dignissimos error exercitationem id ipsa laboriosam laborum
                        laudantium nihil nisi odio, odit omnis optio placeat quam quo, saepe sed similique sunt tempora,
                        tempore vitae voluptates voluptatum!
                    </div>
                    <div>Blanditiis earum provident quas rerum totam. Consectetur delectus distinctio ducimus esse
                        expedita fuga impedit laborum magni, nobis nulla officia officiis, qui sequi, soluta voluptas
                        voluptate voluptates. Atque inventore non reprehenderit?
                    </div>
                    <div>Accusamus alias aliquid culpa dolore, dolores error fuga labore maxime natus neque odio
                        pariatur, quae reiciendis repellendus sapiente veniam voluptatum. Animi aperiam beatae
                        consequatur dignissimos error iste odit quod ut?
                    </div>
                    <div>Consequatur cumque cupiditate est exercitationem. Aspernatur at est exercitationem illo
                        incidunt inventore officiis, perferendis sit sunt tempora. Beatae corporis dolor excepturi fuga
                        ipsa maxime minima modi nesciunt perspiciatis provident, quod!
                    </div>
                    <div>Ad blanditiis ea exercitationem fugiat hic, laborum obcaecati quis sunt. Architecto asperiores,
                        aspernatur consectetur eveniet, explicabo impedit magnam mollitia nobis, nostrum officia
                        reiciendis similique sunt suscipit voluptatem voluptatum. A, laborum.
                    </div>
                    <div>Ab aspernatur autem consequuntur corporis cumque delectus dicta ea exercitationem harum id
                        illum impedit itaque iusto maxime minus molestias nihil odio officiis, possimus qui quidem
                        sapiente similique sit voluptas voluptate?
                    </div>
                    <div>Adipisci aliquam aperiam cumque ipsam, nisi recusandae! Ad at beatae consectetur deserunt ea
                        et, exercitationem harum illo illum impedit ipsa laudantium, minima molestiae nemo, neque
                        nostrum quae qui quidem quo.
                    </div>
                    <div>Architecto obcaecati placeat voluptate? Aliquam porro, sint! At aut, culpa dolorum labore
                        laboriosam libero, molestias necessitatibus nostrum optio quae quod repellat tempora. Atque
                        doloremque dolores expedita perferendis perspiciatis ratione, suscipit?
                    </div></div>
                <a href="#" class="more-link"><span class="more-link__text">подробнее</span></a>
            </article>
            <div class="goods">
                <div class="goods__list">
                    <div class="goods__item">
                        <figure class="goods__container">
                            <p class="goods__number">№7</p>
                            <a href="/product.php" class="goods__image"><img src="/assets/dist/img/anim1.png" alt="" class="goods__thumb"></a>
                            <figcaption class="goods__title"><a href="/product.php" class="goods__link">Гост №7 380 х 253 х 237</a></figcaption>
                        </figure>
                        <a href="/product.php" class="more-link more-link_goods"><span class="more-link__text more-link__text_goods">подробнее</span></a>
                    </div>
                    <div class="goods__item">
                        <figure class="goods__container">
                            <p class="goods__number">№17</p>
                            <a href="/product.php" class="goods__image"><img src="/assets/dist/img/anim1.png" alt="" class="goods__thumb"></a>
                            <figcaption class="goods__title"><a href="#" class="goods__link">Гост №17 380 х 285 х 225</a></figcaption>
                        </figure>
                        <p class="goods__price">17,61</p>
                        <a href="#" class="more-link more-link_fast modal-btn modal-btn_fast"><span class="more-link__text more-link__text_fast">быстрый заказ</span></a>
                        <a href="/product.php" class="more-link more-link_goods"><span class="more-link__text more-link__text_goods">подробнее</span></a>
                    </div>
                    <div class="goods__item">
                        <figure class="goods__container">
                            <p class="goods__number">№18</p>
                            <a href="/product.php" class="goods__image"><img src="/assets/dist/img/anim1.png" alt="" class="goods__thumb"></a>
                            <figcaption class="goods__title"><a href="/product.php" class="goods__link">Гост №18 600 х 320 х 340</a></figcaption>
                        </figure>
                        <p class="goods__price">32.03</p>
                        <a href="#" class="more-link more-link_fast modal-btn modal-btn_fast"><span class="more-link__text more-link__text_fast">быстрый заказ</span></a>
                        <a href="/product.php" class="more-link more-link_goods"><span class="more-link__text more-link__text_goods">подробнее</span></a>
                    </div>
                    <div class="goods__item">
                        <figure class="goods__container">
                            <p class="goods__number">№21</p>
                            <a href="/product.php" class="goods__image"><img src="/assets/dist/img/anim1.png" alt="" class="goods__thumb"></a>
                            <figcaption class="goods__title"><a href="#" class="goods__link">Гост №21 380 х 380 х 228</a></figcaption>
                        </figure>
                        <p class="goods__price">23.72</p>
                        <a href="#" class="more-link more-link_fast modal-btn modal-btn_fast"><span class="more-link__text more-link__text_fast">быстрый заказ</span></a>
                        <a href="/product.php" class="more-link more-link_goods"><span class="more-link__text more-link__text_goods">подробнее</span></a>
                    </div>
                    <div class="goods__item">
                        <figure class="goods__container">
                            <p class="goods__number">№38</p>
                            <a href="/product.php" class="goods__image"><img src="/assets/dist/img/anim1.png" alt="" class="goods__thumb"></a>
                            <figcaption class="goods__title"><a href="/product.php" class="goods__link">Гост №38 380 х 304 х 285</a></figcaption>
                        </figure>
                        <p class="goods__price">20.79</p>
                        <a href="#" class="more-link more-link_fast modal-btn modal-btn_fast"><span class="more-link__text more-link__text_fast">быстрый заказ</span></a>
                        <a href="/product.php" class="more-link more-link_goods"><span class="more-link__text more-link__text_goods">подробнее</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="stages">
            <div class="container container_stages">
                <p class="stages__title">этапы<span class="stages__title_b"> производственного процесса</span></p>
                <ul class="stages__list">
                    <li class="stages__item">
                        <div class="stages__item-text">Разработка конструкции гофротары</div>
                    </li>
                    <li class="stages__item stages__item_stamp">
                        <div class="stages__item-text">Изготовление штампов для сложных изделий</div>
                    </li>
                    <li class="stages__item stages__item_form">
                        <div class="stages__item-text">Заказ печатной формы <br>(1-4 цвета)</div>
                    </li>
                    <li class="stages__item stages__item_box">
                        <div class="stages__item-text">Изготовление гофротары (нанесение печати)</div>
                    </li>
                    <li class="stages__item stages__item_delivery">
                        <div class="stages__item-text">Доставка в любую точку России</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="produce">
                <div class="produce__caption">
                    мы<span class="produce__caption produce__caption_b"> производим</span>
                </div>
                <div class="produce__list">
                    <div class="produce__item">
                        <div class="produce__inner produce__inner_long">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim1.png" alt="Гофротара для..." class="produce__img">
                                    <figcaption class="produce__title">Гофротара для...</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">пищевых продуктов</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">мебели</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">кондитерских изделий</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">пиццы</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">конфет</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">печенья</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">бутылок на заказ</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гофрокороба архивные и папки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Яиц</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">овощей</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim8.png" alt="4-х клапанные гофрокороба" class="produce__img">
                                    <figcaption class="produce__title">4-х клапанные гофрокороба</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Короба для алкогольной продукции</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Короба для пищевой продукции</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Для продуктов и бытовой химии</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner produce__inner_long">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim4.png" alt="самосборные гофрокороба" class="produce__img">
                                    <figcaption class="produce__title">самосборные гофрокороба</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Архивные короба и папки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Овощные лотки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Лотки для птицы</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Лотки для рыбы</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Лотки для кондитерских изделий</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Короба для пиццы</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Короба для обуви</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гофрокороба с ручкой</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гофрокороба сложной высечки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">шоубоксы</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">лотки для мяса</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim1.png" alt="короба гост" class="produce__img">
                                    <figcaption class="produce__title">короба гост</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гост №18 630х320х240</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гост №38 380х304х285</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гост №17 380х285х225</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гост №7 380х253х237</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гост №21 380х380х228</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure produce__figure_empty">
                                    <img src="assets/dist/img/anim5.png" alt="Крупноформатные гофрокороба" class="produce__img">
                                    <figcaption class="produce__title">Крупноформатные гофрокороба</figcaption>
                                </figure>
                            </a>

                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure produce__figure_empty">
                                    <img src="assets/dist/img/anim1.png" alt="Многоточечная склейка" class="produce__img">
                                    <figcaption class="produce__title">Многоточечная склейка</figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim2.png" alt="комплектующие к гофрокоробам" class="produce__img">
                                    <figcaption class="produce__title">комплектующие к гофрокоробам</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гофропрокладки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Гофровкладыши</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Обечайки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Решетки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Ложементы</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim7.png" alt="прочие изделия" class="produce__img">
                                    <figcaption class="produce__title">прочие изделия</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Уголки</a></li>
                                <li class="produce__sub-item"><a href="" class="produce__sub-link">Круги</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner produce__inner_right">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim4.png" alt="гофрокартон в листах" class="produce__img">
                                    <figcaption class="produce__title">гофрокартон в листах</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub produce__sub_right">
                                <li class="produce__sub-item produce__sub-item_right"><a href="#" class="produce__sub-link">Микрогофрокартон</a></li>
                                <li class="produce__sub-item produce__sub-item_right"><a href="#" class="produce__sub-link">Тлехслойный гофрокартон</a></li>
                                <li class="produce__sub-item produce__sub-item_right"><a href="#" class="produce__sub-link">Пятислойный гофрокартон</a></li>
                                <li class="produce__sub-item produce__sub-item_right"><a href="#" class="produce__sub-link">Беленый гофрокартон</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="produce__item">
                        <div class="produce__inner">
                            <a href="#" class="produce__link">
                                <figure class="produce__figure">
                                    <img src="assets/dist/img/anim6.png" alt="Дополнительные услуги" class="produce__img">
                                    <figcaption class="produce__title">Дополнительные услуги</figcaption>
                                </figure>
                            </a>
                            <ul class="produce__sub">
                                <li class="produce__sub-item"><a href="#" class="produce__sub-link">Разработка конструкций</a></li>
                                <li class="produce__sub-item"><a href="#" class="produce__sub-link">Изготовление штанц-форм</a></li>
                                <li class="produce__sub-item"><a href="#" class="produce__sub-link">Изготовление печатных форм</a></li>
                                <li class="produce__sub-item"><a href="#" class="produce__sub-link">Изготовление образцов</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="f-block">
            <div class="container container_form">
                <p class="f-block__caption">Не нашли подходящей гофротары?<span class="f-block__caption_b">Сделаем на заказ!</span>
                </p>
                <div class="f-block__inner">
                    <ul class="advantages">
                        <li class="advantages__item"><span class="advantages__text">Собственное производство во <br>Владимирской области</span>
                        </li>
                        <li class="advantages__item advantages__item_clock"><span class="advantages__text">15 лет на рынке гофроупаковки</span>
                        </li>
                        <li class="advantages__item advantages__item_map"><span class="advantages__text">4 линии производства гофрокартона</span>
                        </li>
                        <li class="advantages__item advantages__item_cmyk"><span class="advantages__text">Современное полноцветное <br>флексопечатное оборудование (CMYK)</span>
                        </li>
                        <li class="advantages__item advantages__item_truck"><span class="advantages__text">Оперативная доставка гофротары, <br>собственным транспортом</span>
                        </li>
                        <li class="advantages__item advantages__item_deadline"><span class="advantages__text">Сроки выполнения заказа от 2-х дней</span>
                        </li>
                    </ul>
                    <form method="post" action="#" class="f-ordering">
                        <div class="f-ordering__column f-ordering__hide">
                            <p class="f-ordering__row">
                                <label for="f-ordering__type" class="f-ordering__label">Тип короба</label>
                                <span class="f-ordering__select-wrap">
                                    <select name="type" id="f-ordering__type" class="f-ordering__select">
                                        <option value="4-х клапанный">4-х клапанный</option>
                                        <option value="4-х клапанный">2-х клапанный</option>
                                        <option value="4-х клапанный">5-х клапанный</option>
                                    </select>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label class="f-ordering__label">Размеры (Д - Ш - В)</label>
                                <span class="f-ordering__row-inner">
                                    <input type="text" name="length" class="f-ordering__input f-ordering__input_small">
                                    <input type="text" name="width" class="f-ordering__input f-ordering__input_small">
                                    <input type="text" name="height" class="f-ordering__input f-ordering__input_small">
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="mark_name" class="f-ordering__label">Марка картона</label>
                                <span class="f-ordering__row-inner">
                                    <input type="text" name="mark_name" id="mark_name"
                                           class="f-ordering__input f-ordering__input_middle">
                                    <span class="f-ordering__select-wrap f-ordering__select-wrap_middle">
                                        <select name="mark_type" class="f-ordering__select">
                                            <option value="Бурый">Бурый</option>
                                            <option value="Серый">Серый</option>
                                            <option value="Белый">Белый</option>
                                        </select>
                                    </span>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__profile" class="f-ordering__label">Профиль картона</label>
                                <span class="f-ordering__select-wrap">
                                    <select name="profile" id="f-ordering__profile" class="f-ordering__select">
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                    </select>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__print" class="f-ordering__label">Печать</label>
                                <span class="f-ordering__select-wrap">
                                    <select name="print" id="f-ordering__print" class="f-ordering__select">
                                            <option value="yes">Да</option>
                                            <option value="no">Нет</option>
                                    </select>
                                </span>
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__color" class="f-ordering__label">Количество цветов</label>
                                <input type="text" name="color" id="f-ordering__color" class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__square" class="f-ordering__label">Площадь запечатки</label>
                                <input type="text" name="square" id="f-ordering__square" class="f-ordering__input">
                            </p>
                        </div>
                        <div class="f-ordering__column">
                            <p class="f-ordering__row f-ordering__hide">
                                <label for="f-ordering__circulation" class="f-ordering__label">Тираж</label>
                                <input type="text" name="circulation" id="f-ordering__circulation"
                                       class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row f-ordering__hide">
                                <label for="f-ordering__comment" class="f-ordering__label">Комментарий</label>
                                <input type="text" name="comment" id="f-ordering__comment" class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__org" class="f-ordering__label">Организация</label>
                                <input type="text" name="org" id="f-ordering__org" class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__contact" class="f-ordering__label">Контактное лицо</label>
                                <input type="text" name="contact" required id="f-ordering__contact"
                                       class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row f-ordering__hide">
                                <label for="f-ordering__email" class="f-ordering__label">E-mail</label>
                                <input type="email" name="email" required id="f-ordering__email"
                                       class="f-ordering__input">
                            </p>
                            <p class="f-ordering__row">
                                <label for="f-ordering__phone" class="f-ordering__label">Телефон</label>
                                <input type="text" name="phone" placeholder="+7 (___)-___-__-__" id="f-ordering__phone"
                                       class="phone-mask f-ordering__input">
                            </p>
                            <p class="f-ordering__row f-ordering__row_submit">
                                <button class="f-ordering__submit"><span
                                        class="f-ordering__submit-text">отправить</span></button>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
<?php require_once __DIR__.'/footer.php'?>
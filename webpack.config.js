const webpack = require('webpack');

module.exports = {
    output: {
        /*        library: 'Library',*/
        filename: 'app.js',
        /*        libraryTarget: 'umd'*/
    },
    module: {
        loaders: [{
            loader: 'babel-loader'
        }]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
            },
            output: {
                comments: false,
            },
        }),
    ]
}